package com.common;

import com.persistance.AgeGroupInfo;
import com.persistance.BrandInfo;
import com.persistance.CategoryInfo;
import com.persistance.ColorInfo;
import com.persistance.CompanyInfo;
import com.persistance.CurrentStockInfo;
import com.persistance.CustomerInfo;
import com.persistance.ExchangeChildInfo;
import com.persistance.ExchangeMasterInfo;
import com.persistance.GenderGroupInfo;
import com.persistance.InvoiceChildInfo;
import com.persistance.OrderReceiveInfo;
import com.persistance.ProductInfo;
import com.persistance.ProductOrderDetailsInfo;
import com.persistance.ProductOrderInfo;
import com.persistance.InvoiceMasterInfo;
import com.persistance.ProductReturnInfo;
import com.persistance.SizeInfo;
import com.persistance.SubCategoryInfo;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CommonList {

    public ArrayList<UserInfo> loginInfo(Connection connection, String email, String password) {

        UserInfo userInfo = null;
        ArrayList<UserInfo> userInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLoginInfo(email, password));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();
                    //
                    userInfo.setUserId(rs.getInt("user_id"));
                    userInfo.setFirstName(rs.getString("first_name"));
                    userInfo.setLastName(rs.getString("last_name"));
                    userInfo.setUserEmail(rs.getString("user_email"));
                    userInfo.setUserPassword(rs.getString("user_password"));

                    userInfo.setUserPhone(rs.getString("user_phone"));
                    userInfo.setUserCountry(rs.getString("user_country"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    userInfo.setInsertDate(rs.getString("insert_date"));

                    userInfo.setUpdateBy(rs.getString("update_by"));
                    userInfo.setUpdateDate(rs.getString("update_date"));
                    userInfo.setLastLogin(rs.getString("last_login"));
                    userInfo.setLastLogout(rs.getString("last_logout"));
                    userInfo.setSuspendStatus(rs.getString("suspend_status").charAt(0));
                    //
                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public ArrayList<SizeInfo> getSizeInfo(Connection connection) {

        SizeInfo sizeInfo = null;
        CompanyInfo companyInfo = null;
        ArrayList<SizeInfo> sizeInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSizeInfo());
                rs = ps.executeQuery();

                sizeInfoList = new ArrayList<SizeInfo>();

                while (rs.next()) {

                    sizeInfo = new SizeInfo();
                    companyInfo = new CompanyInfo();

                    sizeInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    sizeInfo.setSizeUnit(rs.getString("size_unit"));
                    sizeInfo.setSize0(rs.getString("size0"));
                    sizeInfo.setSize1(rs.getString("size1"));
                    sizeInfo.setSize2(rs.getString("size2"));
                    sizeInfo.setSize3(rs.getString("size3"));
                    sizeInfo.setSize4(rs.getString("size4"));
                    sizeInfo.setSize5(rs.getString("size5"));
                    sizeInfo.setSize6(rs.getString("size6"));
                    sizeInfo.setSize7(rs.getString("size7"));
                    sizeInfo.setSize8(rs.getString("size8"));
                    sizeInfo.setSize9(rs.getString("size9"));
                    sizeInfo.setSizeDescription(rs.getString("size_desc"));
                    sizeInfo.setSizeStatus(rs.getString("size_status").charAt(0));

                    companyInfo.setCompanyId(rs.getInt("company_id"));

                    sizeInfo.setCompanyInfo(companyInfo);

                    sizeInfoList.add(sizeInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                sizeInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return sizeInfoList;
    }

    public ArrayList<BrandInfo> getBrandInfo(Connection connection) {

        BrandInfo brandInfo = null;
        CompanyInfo companyInfo = null;
        ArrayList<BrandInfo> brandInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectBrandInfo());
                rs = ps.executeQuery();

                brandInfoList = new ArrayList<BrandInfo>();

                while (rs.next()) {

                    brandInfo = new BrandInfo();
                    companyInfo = new CompanyInfo();

                    brandInfo.setBrandCode(rs.getString("brand_code"));
                    brandInfo.setBrandName(rs.getString("brand_name"));
                    brandInfo.setBrandDescription(rs.getString("brand_description"));
                    brandInfo.setBrandStatus(rs.getString("brand_status").charAt(0));

                    companyInfo.setCompanyId(rs.getInt("company_id"));

                    brandInfo.setCompanyInfo(companyInfo);

                    brandInfoList.add(brandInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                brandInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return brandInfoList;
    }

    public ArrayList<CategoryInfo> getCategoryInfo(Connection connection) {

        CategoryInfo categoryInfo = null;
        CompanyInfo companyInfo = null;
        ArrayList<CategoryInfo> categoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCategoryInfo());
                rs = ps.executeQuery();

                categoryInfoList = new ArrayList<CategoryInfo>();

                while (rs.next()) {

                    categoryInfo = new CategoryInfo();
                    companyInfo = new CompanyInfo();

                    categoryInfo.setCategoryCode(rs.getString("category_code"));
                    categoryInfo.setCategoryName(rs.getString("category_name"));
                    categoryInfo.setCategoryDescription(rs.getString("category_description"));
                    categoryInfo.setCategoryStatus(rs.getString("category_status").charAt(0));

                    companyInfo.setCompanyId(rs.getInt("company_id"));

                    categoryInfo.setCompanyInfo(companyInfo);

                    categoryInfoList.add(categoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                categoryInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return categoryInfoList;
    }

    public ArrayList<SubCategoryInfo> getSubCategoryInfo(Connection connection) {

        SubCategoryInfo subCategoryInfo = null;
        CategoryInfo categoryInfo = null;
        CompanyInfo companyInfo = null;
        ArrayList<SubCategoryInfo> subCategoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSubCategoryInfo());
                rs = ps.executeQuery();

                subCategoryInfoList = new ArrayList<SubCategoryInfo>();

                while (rs.next()) {

                    subCategoryInfo = new SubCategoryInfo();
                    categoryInfo = new CategoryInfo();
                    companyInfo = new CompanyInfo();

                    subCategoryInfo.setSubCategoryCode(rs.getString("sub_category_code"));
                    subCategoryInfo.setSubCategoryName(rs.getString("sub_category_name"));
                    subCategoryInfo.setSubCategoryDescription(rs.getString("sub_category_description"));
                    subCategoryInfo.setSubCategoryStatus(rs.getString("sub_category_status").charAt(0));

                    categoryInfo.setCategoryCode(rs.getString("category_code"));
                    categoryInfo.setCategoryName(rs.getString("category_name"));
                    subCategoryInfo.setCategoryInfo(categoryInfo);

                    companyInfo.setCompanyId(rs.getInt("com_id"));
                    subCategoryInfo.setCompanyInfo(companyInfo);

                    subCategoryInfoList.add(subCategoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                subCategoryInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return subCategoryInfoList;
    }

    public ArrayList<ProductInfo> getProductInfo(Connection connection) {

        ProductInfo productInfo = null;
        ArrayList<ProductInfo> productInfoList = null;
        //        
        BrandInfo brandInfo = null;
        SizeInfo sizeInfo = null;
        SubCategoryInfo subCategoryInfo = null;
        CategoryInfo categoryInfo = null;
        ColorInfo colorInfo = null;
        AgeGroupInfo ageGroupInfo = null;
        GenderGroupInfo genderGroupInfo = null;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductInfo());
                rs = ps.executeQuery();

                productInfoList = new ArrayList<ProductInfo>();

                while (rs.next()) {

                    productInfo = new ProductInfo();
                    //
                    sizeInfo = new SizeInfo();
                    brandInfo = new BrandInfo();
                    categoryInfo = new CategoryInfo();
                    subCategoryInfo = new SubCategoryInfo();
                    companyInfo = new CompanyInfo();

                    colorInfo = new ColorInfo();
                    ageGroupInfo = new AgeGroupInfo();
                    genderGroupInfo = new GenderGroupInfo();
                    //
                    productInfo.setItemCode(rs.getString("pro_item_code"));
                    productInfo.setItemCostPrice(rs.getDouble("pro_cost_price"));
                    productInfo.setItemVat(rs.getDouble("pro_vat"));
                    productInfo.setItemNetPrice(rs.getDouble("pro_net_price"));
                    productInfo.setItemMRP(rs.getDouble("pro_mrp"));
                    productInfo.setItemLifeCycle(rs.getDate("pro_life_cycle"));
                    productInfo.setItemStatus(rs.getString("pro_status").charAt(0));
                    productInfo.setItemSupplier(rs.getString("pro_supplier"));
                    productInfo.setItemSuppReference(rs.getString("pro_supp_reference"));
                    productInfo.setItemState(rs.getString("pro_state"));
                    productInfo.setItemTemperature(rs.getString("pro_temp"));
                    productInfo.setItemDescription(rs.getString("pro_desc"));
                    productInfo.setItemImageURL(rs.getString("pro_image_url"));
                    //    
                    sizeInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    sizeInfo.setSizeUnit(rs.getString("size_unit"));
                    productInfo.setSizeInfo(sizeInfo);
                    //                    
                    brandInfo.setBrandCode(rs.getString("brand_code"));
                    brandInfo.setBrandName(rs.getString("brand_name"));
                    productInfo.setBrandInfo(brandInfo);
                    //    
                    categoryInfo.setCategoryCode(rs.getString("category_code"));
                    categoryInfo.setCategoryName(rs.getString("category_name"));
                    productInfo.setCategoryInfo(categoryInfo);
                    //   
                    subCategoryInfo.setSubCategoryCode(rs.getString("sub_category_code"));
                    subCategoryInfo.setSubCategoryName(rs.getString("sub_category_name"));
                    productInfo.setSubCategoryInfo(subCategoryInfo);
                    //                    
                    companyInfo.setCompanyId(rs.getInt("com_id"));
                    companyInfo.setCompanyName(rs.getString("com_name"));
                    productInfo.setCompanyInfo(companyInfo);
                    //                    
                    colorInfo.setColorCode(rs.getString("color_code"));
                    colorInfo.setColorName(rs.getString("color_name"));
                    productInfo.setColorInfo(colorInfo);
                    //                    
                    ageGroupInfo.setAgeCode(rs.getString("age_group_code"));
                    ageGroupInfo.setAgeCodeName(rs.getString("age_group_name"));
                    productInfo.setAgeGroupInfo(ageGroupInfo);
                    //          
                    genderGroupInfo.setGenderCode(rs.getString("gender_group_code"));
                    genderGroupInfo.setGenderCodeName(rs.getString("gender_group_name"));
                    productInfo.setGenderGroupInfo(genderGroupInfo);
                    //   
                    productInfoList.add(productInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productInfoList;
    }

    public ArrayList<ComboValue> getComboValue(Connection connection, String selectStatement) {

        ArrayList<ComboValue> comboValuesList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                comboValuesList = new ArrayList<ComboValue>();

                while (rs.next()) {
                    comboValuesList.add(new ComboValue(rs.getString("COMBO_KEY"), rs.getString("COMBO_VALUE")));
                }
            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return comboValuesList;
    }

    public ArrayList<ProductInfo> searchProductEnrtyInfo(Connection connection, String searchField, String searchValue) {

        ProductInfo productInfo = null;
        ArrayList<ProductInfo> searchProductInfoList = null;
        //        
        BrandInfo brandInfo = null;
        SizeInfo sizeInfo = null;
        SubCategoryInfo subCategoryInfo = null;
        CategoryInfo categoryInfo = null;
        ColorInfo colorInfo = null;
        AgeGroupInfo ageGroupInfo = null;
        GenderGroupInfo genderGroupInfo = null;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchProductInfo(searchField, searchValue));
                rs = ps.executeQuery();

                searchProductInfoList = new ArrayList<ProductInfo>();

                while (rs.next()) {

                    productInfo = new ProductInfo();
                    //
                    sizeInfo = new SizeInfo();
                    brandInfo = new BrandInfo();
                    categoryInfo = new CategoryInfo();
                    subCategoryInfo = new SubCategoryInfo();
                    companyInfo = new CompanyInfo();

                    colorInfo = new ColorInfo();
                    ageGroupInfo = new AgeGroupInfo();
                    genderGroupInfo = new GenderGroupInfo();
                    //
                    productInfo.setItemCode(rs.getString("pro_item_code"));
                    productInfo.setItemCostPrice(rs.getDouble("pro_cost_price"));
                    productInfo.setItemVat(rs.getDouble("pro_vat"));
                    productInfo.setItemNetPrice(rs.getDouble("pro_net_price"));
                    productInfo.setItemMRP(rs.getDouble("pro_mrp"));
                    productInfo.setItemLifeCycle(rs.getDate("pro_life_cycle"));
                    productInfo.setItemStatus(rs.getString("pro_status").charAt(0));
                    productInfo.setItemSupplier(rs.getString("pro_supplier"));
                    productInfo.setItemSuppReference(rs.getString("pro_supp_reference"));
                    productInfo.setItemState(rs.getString("pro_state"));
                    productInfo.setItemTemperature(rs.getString("pro_temp"));
                    productInfo.setItemDescription(rs.getString("pro_desc"));
                    productInfo.setItemImageURL(rs.getString("pro_image_url"));
                    //    
                    sizeInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    sizeInfo.setSizeUnit(rs.getString("size_unit"));
                    productInfo.setSizeInfo(sizeInfo);
                    //                    
                    brandInfo.setBrandCode(rs.getString("brand_code"));
                    brandInfo.setBrandName(rs.getString("brand_name"));
                    productInfo.setBrandInfo(brandInfo);
                    //    
                    categoryInfo.setCategoryCode(rs.getString("category_code"));
                    categoryInfo.setCategoryName(rs.getString("category_name"));
                    productInfo.setCategoryInfo(categoryInfo);
                    //   
                    subCategoryInfo.setSubCategoryCode(rs.getString("sub_category_code"));
                    subCategoryInfo.setSubCategoryName(rs.getString("sub_category_name"));
                    productInfo.setSubCategoryInfo(subCategoryInfo);
                    //                    
                    companyInfo.setCompanyId(rs.getInt("com_id"));
                    companyInfo.setCompanyName(rs.getString("com_name"));
                    productInfo.setCompanyInfo(companyInfo);
                    //                    
                    colorInfo.setColorCode(rs.getString("color_code"));
                    colorInfo.setColorName(rs.getString("color_name"));
                    productInfo.setColorInfo(colorInfo);
                    //                    
                    ageGroupInfo.setAgeCode(rs.getString("age_group_code"));
                    ageGroupInfo.setAgeCodeName(rs.getString("age_group_name"));
                    productInfo.setAgeGroupInfo(ageGroupInfo);
                    //          
                    genderGroupInfo.setGenderCode(rs.getString("gender_group_code"));
                    genderGroupInfo.setGenderCodeName(rs.getString("gender_group_name"));
                    productInfo.setGenderGroupInfo(genderGroupInfo);
                    //   
                    searchProductInfoList.add(productInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                searchProductInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return searchProductInfoList;
    }

    public ArrayList<ProductOrderInfo> getProductOrderInfo(Connection connection) {

        ProductOrderInfo productOrderInfo = null;
        ArrayList<ProductOrderInfo> productOrderInfoList = null;
        //        
        ProductOrderDetailsInfo productOrderDetailsInfo;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductOrderInfo());
                rs = ps.executeQuery();

                productOrderInfoList = new ArrayList<ProductOrderInfo>();

                while (rs.next()) {

                    productOrderInfo = new ProductOrderInfo();
                    //
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    companyInfo = new CompanyInfo();

                    productOrderInfo.setpPurchaseOrderNo(rs.getString("product_order_no"));
                    productOrderInfo.setpPurchaseOrderDate(rs.getString("purchase_order_date"));
                    productOrderInfo.setSupplierID(rs.getString("supplier_id"));
                    productOrderInfo.setSupplierCompany(rs.getString("supplier_company"));
                    productOrderInfo.setSupplierPhone(rs.getString("supplier_phone"));
                    productOrderInfo.setSupplierEmail(rs.getString("supplier_email"));
                    productOrderInfo.setPuachaseOrderStatus(rs.getString("puachase_order_status").charAt(0));

                    productOrderDetailsInfo.setItemCode(rs.getString("item_code"));
                    productOrderDetailsInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    productOrderDetailsInfo.setNetPrice((rs.getDouble("net_price")) == 0.0 ? 0.0 : (rs.getDouble("net_price")));
                    productOrderDetailsInfo.setQuantity((rs.getInt("quantity")) == 0 ? 0 : (rs.getInt("quantity")));
                    productOrderDetailsInfo.setS0(rs.getInt("s0"));
                    productOrderDetailsInfo.setS1(rs.getInt("s1"));
                    productOrderDetailsInfo.setS2(rs.getInt("s2"));
                    productOrderDetailsInfo.setS3(rs.getInt("s3"));
                    productOrderDetailsInfo.setS4(rs.getInt("s4"));
                    productOrderDetailsInfo.setS5(rs.getInt("s5"));
                    productOrderDetailsInfo.setS6(rs.getInt("s6"));
                    productOrderDetailsInfo.setS7(rs.getInt("s7"));
                    productOrderDetailsInfo.setS8(rs.getInt("s8"));
                    productOrderDetailsInfo.setS9(rs.getInt("s9"));
                    productOrderDetailsInfo.setSubTotal(rs.getDouble("sub_total"));

                    productOrderInfo.setProductOrderDetailsInfo(productOrderDetailsInfo);

                    companyInfo.setCompanyId(rs.getInt("com_id"));
                    companyInfo.setCompanyName(rs.getString("com_name"));
                    productOrderInfo.setCompanyInfo(companyInfo);

                    productOrderInfoList.add(productOrderInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productOrderInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productOrderInfoList;
    }

    public ArrayList<ProductOrderInfo> searchProductOrderInfo(Connection connection) {

        ProductOrderInfo productPurchaseOrderHistoryInfo = null;
        ArrayList<ProductOrderInfo> productPurchaseInfoList = null;
        //        
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductOrderInfo());
                rs = ps.executeQuery();

                productPurchaseInfoList = new ArrayList<ProductOrderInfo>();

                while (rs.next()) {

                    productPurchaseOrderHistoryInfo = new ProductOrderInfo();
                    //
                    companyInfo = new CompanyInfo();

                    productPurchaseOrderHistoryInfo.setpPurchaseOrderDate(rs.getString("purchase_order_date"));
                    productPurchaseOrderHistoryInfo.setpPurchaseOrderNo(rs.getString("purchase_order_no"));
                    productPurchaseOrderHistoryInfo.setSupplierID(rs.getString("supplier_id"));
                    productPurchaseOrderHistoryInfo.setSupplierCompany(rs.getString("supplier_company"));
                    productPurchaseOrderHistoryInfo.setPuachaseOrderStatus(rs.getString("puachase_order_status").charAt(0));

                    companyInfo.setCompanyId(rs.getInt("com_id"));
                    companyInfo.setCompanyName(rs.getString("com_name"));

                    productPurchaseOrderHistoryInfo.setCompanyInfo(companyInfo);

                    productPurchaseInfoList.add(productPurchaseOrderHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productPurchaseInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productPurchaseInfoList;
    }

    public ArrayList<ProductInfo> getProductSizeInfo(Connection connection, String itemCode) {

        ProductInfo productInfo = null;
        ArrayList<ProductInfo> productSizeInfoList = null;
        //        
        SizeInfo sizeInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductSizeInfo(itemCode));
                rs = ps.executeQuery();

                productSizeInfoList = new ArrayList<ProductInfo>();

                while (rs.next()) {

                    productInfo = new ProductInfo();
                    //
                    sizeInfo = new SizeInfo();
                    //
                    productInfo.setItemCode(rs.getString("pro_item_code"));
                    productInfo.setItemCostPrice(rs.getDouble("pro_cost_price"));
                    productInfo.setItemVat(rs.getDouble("pro_vat"));
                    productInfo.setItemNetPrice(rs.getDouble("pro_net_price"));
                    productInfo.setItemMRP(rs.getDouble("pro_mrp"));
                    //    
                    sizeInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    sizeInfo.setSizeUnit(rs.getString("size_unit"));
                    sizeInfo.setSize0(rs.getString("size0"));
                    sizeInfo.setSize1(rs.getString("size1"));
                    sizeInfo.setSize2(rs.getString("size2"));
                    sizeInfo.setSize3(rs.getString("size3"));
                    sizeInfo.setSize4(rs.getString("size4"));
                    sizeInfo.setSize5(rs.getString("size5"));
                    sizeInfo.setSize6(rs.getString("size6"));
                    sizeInfo.setSize7(rs.getString("size7"));
                    sizeInfo.setSize8(rs.getString("size8"));
                    sizeInfo.setSize9(rs.getString("size9"));
                    productInfo.setSizeInfo(sizeInfo);
                    //   
                    productSizeInfoList.add(productInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productSizeInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productSizeInfoList;
    }

    public ArrayList<ProductOrderInfo> getProductOrderInfoBySearch(Connection connection, String orderNo) {

        ProductOrderInfo productOrderInfo = null;
        ArrayList<ProductOrderInfo> productOrderInfoList = null;
        //        
        ProductOrderDetailsInfo productOrderDetailsInfo;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductOrderInfoBySearch(orderNo));
                rs = ps.executeQuery();

                productOrderInfoList = new ArrayList<ProductOrderInfo>();

                while (rs.next()) {

                    productOrderInfo = new ProductOrderInfo();
                    //
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    companyInfo = new CompanyInfo();

                    productOrderInfo.setpPurchaseOrderDate(rs.getString("purchase_order_date"));
                    productOrderInfo.setpPurchaseOrderNo(rs.getString("product_order_no"));
                    productOrderInfo.setSupplierID(rs.getString("supplier_id"));
                    productOrderInfo.setSupplierCompany(rs.getString("supplier_company"));
                    productOrderInfo.setSupplierPhone(rs.getString("supplier_phone"));
                    productOrderInfo.setSupplierEmail(rs.getString("supplier_email"));
                    productOrderInfo.setPuachaseOrderStatus(rs.getString("puachase_order_status").charAt(0));

                    productOrderDetailsInfo.setItemCode(rs.getString("item_code"));
                    productOrderDetailsInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    productOrderDetailsInfo.setNetPrice((rs.getDouble("net_price")) == 0.0 ? 0.0 : (rs.getDouble("net_price")));
                    productOrderDetailsInfo.setQuantity((rs.getInt("quantity")) == 0 ? 0 : (rs.getInt("quantity")));
                    productOrderDetailsInfo.setS0(rs.getInt("s0"));
                    productOrderDetailsInfo.setS1(rs.getInt("s1"));
                    productOrderDetailsInfo.setS2(rs.getInt("s2"));
                    productOrderDetailsInfo.setS3(rs.getInt("s3"));
                    productOrderDetailsInfo.setS4(rs.getInt("s4"));
                    productOrderDetailsInfo.setS5(rs.getInt("s5"));
                    productOrderDetailsInfo.setS6(rs.getInt("s6"));
                    productOrderDetailsInfo.setS7(rs.getInt("s7"));
                    productOrderDetailsInfo.setS8(rs.getInt("s8"));
                    productOrderDetailsInfo.setS9(rs.getInt("s9"));
                    productOrderDetailsInfo.setSubTotal(rs.getDouble("sub_total"));
                    productOrderDetailsInfo.setOrderStatus(rs.getString("order_status").charAt(0));
                    productOrderInfo.setProductOrderDetailsInfo(productOrderDetailsInfo);

                    companyInfo.setCompanyId(rs.getInt("com_id"));
                    companyInfo.setCompanyName(rs.getString("com_name"));
                    productOrderInfo.setCompanyInfo(companyInfo);

                    productOrderInfoList.add(productOrderInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productOrderInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productOrderInfoList;
    }

    public ArrayList<OrderReceiveInfo> getProductOrderReceiveInfoBySearch(Connection connection, String orderNo) {

        OrderReceiveInfo orderReceiveInfo = null;
        ArrayList<OrderReceiveInfo> productOrderReceiveInfoList = null;
        //       
        ProductOrderInfo productOrderInfo = null;
        ProductOrderDetailsInfo productOrderDetailsInfo;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductOrderReceiveInfoBySearch(orderNo));
                rs = ps.executeQuery();

                productOrderReceiveInfoList = new ArrayList<OrderReceiveInfo>();

                while (rs.next()) {

                    orderReceiveInfo = new OrderReceiveInfo();
                    //                    
                    productOrderInfo = new ProductOrderInfo();
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    companyInfo = new CompanyInfo();
                    //    
                    orderReceiveInfo.setOrderNo(rs.getString("order_no"));
                    orderReceiveInfo.setReceiveDate(rs.getString("order_date"));
                    orderReceiveInfo.setItemCode(rs.getString("item_code"));
                    orderReceiveInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    orderReceiveInfo.setReceiveMRP((rs.getDouble("receive_mrp")) == 0.0 ? 0.0 : (rs.getDouble("receive_mrp")));
                    orderReceiveInfo.setReceiveQuantity((rs.getInt("receive_quantity")) == 0 ? 0 : (rs.getInt("receive_quantity")));
                    orderReceiveInfo.setS0Received(rs.getInt("s0_received"));
                    orderReceiveInfo.setS1Received(rs.getInt("s1_received"));
                    orderReceiveInfo.setS2Received(rs.getInt("s2_received"));
                    orderReceiveInfo.setS3Received(rs.getInt("s3_received"));
                    orderReceiveInfo.setS4Received(rs.getInt("s4_received"));
                    orderReceiveInfo.setS5Received(rs.getInt("s5_received"));
                    orderReceiveInfo.setS6Received(rs.getInt("s6_received"));
                    orderReceiveInfo.setS7Received(rs.getInt("s7_received"));
                    orderReceiveInfo.setS8Received(rs.getInt("s8_received"));
                    orderReceiveInfo.setS9Received(rs.getInt("s9_received"));
                    orderReceiveInfo.setReceiveSubTotal((rs.getDouble("receive_sub_total")) == 0.0 ? 0.0 : (rs.getDouble("receive_sub_total")));
                    orderReceiveInfo.setReceiveStatus(rs.getString("receive_status").charAt(0));

                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date dbDate = rs.getTimestamp("receive_date");
                    String fDate = formatter.format(dbDate);
                    orderReceiveInfo.setReceiveDate(fDate);

                    productOrderReceiveInfoList.add(orderReceiveInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productOrderReceiveInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productOrderReceiveInfoList;
    }

    public ArrayList<OrderReceiveInfo> getProductOrderReceiveInfo(Connection connection) {

        OrderReceiveInfo orderReceiveInfo = null;
        ArrayList<OrderReceiveInfo> productOrderReceiveInfoList = null;
        //       
        ProductOrderInfo productOrderInfo = null;
        ProductOrderDetailsInfo productOrderDetailsInfo;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductOrderReceiveInfo());
                rs = ps.executeQuery();

                productOrderReceiveInfoList = new ArrayList<OrderReceiveInfo>();

                while (rs.next()) {

                    orderReceiveInfo = new OrderReceiveInfo();
                    //                    
                    productOrderInfo = new ProductOrderInfo();
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    companyInfo = new CompanyInfo();
                    //    
                    orderReceiveInfo.setOrderNo(rs.getString("order_no"));
                    orderReceiveInfo.setReceiveDate(rs.getString("order_date"));
                    orderReceiveInfo.setItemCode(rs.getString("item_code"));
                    orderReceiveInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    orderReceiveInfo.setReceiveMRP((rs.getDouble("receive_mrp")) == 0.0 ? 0.0 : (rs.getDouble("receive_mrp")));
                    orderReceiveInfo.setReceiveQuantity((rs.getInt("receive_quantity")) == 0 ? 0 : (rs.getInt("receive_quantity")));
                    orderReceiveInfo.setS0Received(rs.getInt("s0_received"));
                    orderReceiveInfo.setS1Received(rs.getInt("s1_received"));
                    orderReceiveInfo.setS2Received(rs.getInt("s2_received"));
                    orderReceiveInfo.setS3Received(rs.getInt("s3_received"));
                    orderReceiveInfo.setS4Received(rs.getInt("s4_received"));
                    orderReceiveInfo.setS5Received(rs.getInt("s5_received"));
                    orderReceiveInfo.setS6Received(rs.getInt("s6_received"));
                    orderReceiveInfo.setS7Received(rs.getInt("s7_received"));
                    orderReceiveInfo.setS8Received(rs.getInt("s8_received"));
                    orderReceiveInfo.setS9Received(rs.getInt("s9_received"));
                    orderReceiveInfo.setReceiveSubTotal((rs.getDouble("receive_sub_total")) == 0.0 ? 0.0 : (rs.getDouble("receive_sub_total")));
                    orderReceiveInfo.setReceiveStatus(rs.getString("receive_status").charAt(0));
                    orderReceiveInfo.setReceivedBy(rs.getString("received_by"));

                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date dbDate = rs.getTimestamp("receive_date");
                    String fDate = formatter.format(dbDate);
                    orderReceiveInfo.setReceiveDate(fDate);

                    productOrderReceiveInfoList.add(orderReceiveInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productOrderReceiveInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productOrderReceiveInfoList;
    }

    public ArrayList<CustomerInfo> getCustomerInfoBySearch(Connection connection, String cID) {

        CustomerInfo customerInfo;
        ArrayList<CustomerInfo> customerInfoList = null;
        //        
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCustomerInfoBySearch(cID));
                rs = ps.executeQuery();

                customerInfoList = new ArrayList<CustomerInfo>();

                while (rs.next()) {

                    customerInfo = new CustomerInfo();
                    //   
                    customerInfo.setCustomerID(rs.getString("customer_id"));
                    customerInfo.setContactNo(rs.getString("contact_no"));
                    customerInfo.setFullName(rs.getString("full_name"));
                    customerInfo.setDateOfBirth(rs.getString("dob"));
                    customerInfo.setReligion(rs.getString("religion"));
                    customerInfo.setGender(rs.getString("gender"));
                    customerInfo.setHotLine(rs.getString("hot_line"));
                    customerInfo.setAddress(rs.getString("address"));

                    customerInfoList.add(customerInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                customerInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return customerInfoList;
    }

    public ArrayList<ProductInfo> getProductDetailsForInvoice(Connection connection, String itemCode) {

        ProductInfo productInfo = null;
        CurrentStockInfo currentStockInfo = null;
        ProductOrderDetailsInfo productOrderDetailsInfo = null;
        ArrayList<ProductInfo> productInfoList = null;
        //        
        BrandInfo brandInfo = null;
        SizeInfo sizeInfo = null;
        SubCategoryInfo subCategoryInfo = null;
        CategoryInfo categoryInfo = null;

        ColorInfo colorInfo = null;
        AgeGroupInfo ageGroupInfo = null;
        GenderGroupInfo genderGroupInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductDetailsForInvoice(itemCode));
                rs = ps.executeQuery();

                productInfoList = new ArrayList<ProductInfo>();

                while (rs.next()) {

                    productInfo = new ProductInfo();
                    currentStockInfo = new CurrentStockInfo();
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    //
                    sizeInfo = new SizeInfo();
                    brandInfo = new BrandInfo();
                    categoryInfo = new CategoryInfo();
                    subCategoryInfo = new SubCategoryInfo();

                    colorInfo = new ColorInfo();
                    ageGroupInfo = new AgeGroupInfo();
                    genderGroupInfo = new GenderGroupInfo();
                    //
                    productInfo.setItemCode(rs.getString("pro_item_code"));
                    productInfo.setProductName(rs.getString("pro_name"));
                    productInfo.setItemCostPrice(rs.getDouble("pro_cost_price"));
                    productInfo.setItemVat(rs.getDouble("pro_vat"));
                    productInfo.setItemNetPrice(rs.getDouble("pro_net_price"));
                    productInfo.setItemMRP(rs.getDouble("pro_mrp"));
                    productInfo.setItemLifeCycle(rs.getDate("pro_life_cycle"));
                    productInfo.setItemStatus(rs.getString("pro_status").charAt(0));
                    productInfo.setItemSupplier(rs.getString("pro_supplier"));
                    productInfo.setItemSuppReference(rs.getString("pro_supp_reference"));
                    productInfo.setItemState(rs.getString("pro_state"));
                    productInfo.setItemTemperature(rs.getString("pro_temp"));
                    productInfo.setItemDescription(rs.getString("pro_desc"));
                    productInfo.setItemImageURL(rs.getString("pro_image_url"));
                    //    
                    sizeInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    sizeInfo.setSizeUnit(rs.getString("size_unit"));
                    sizeInfo.setSize0(rs.getString("size0"));
                    sizeInfo.setSize1(rs.getString("size1"));
                    sizeInfo.setSize2(rs.getString("size2"));
                    sizeInfo.setSize3(rs.getString("size3"));
                    sizeInfo.setSize4(rs.getString("size4"));
                    sizeInfo.setSize5(rs.getString("size5"));
                    sizeInfo.setSize6(rs.getString("size6"));
                    sizeInfo.setSize7(rs.getString("size7"));
                    sizeInfo.setSize8(rs.getString("size8"));
                    sizeInfo.setSize9(rs.getString("size9"));
                    productInfo.setSizeInfo(sizeInfo);
                    //                    
                    brandInfo.setBrandCode(rs.getString("brand_code"));
                    brandInfo.setBrandName(rs.getString("brand_name"));
                    productInfo.setBrandInfo(brandInfo);
                    //    
                    categoryInfo.setCategoryCode(rs.getString("category_code"));
                    categoryInfo.setCategoryName(rs.getString("category_name"));
                    productInfo.setCategoryInfo(categoryInfo);
                    //   
                    subCategoryInfo.setSubCategoryCode(rs.getString("sub_category_code"));
                    subCategoryInfo.setSubCategoryName(rs.getString("sub_category_name"));
                    productInfo.setSubCategoryInfo(subCategoryInfo);
                    //   
                    currentStockInfo.setSize0(rs.getInt("stock_size0"));
                    currentStockInfo.setSize1(rs.getInt("stock_size1"));
                    currentStockInfo.setSize2(rs.getInt("stock_size2"));
                    currentStockInfo.setSize3(rs.getInt("stock_size3"));
                    currentStockInfo.setSize4(rs.getInt("stock_size4"));
                    currentStockInfo.setSize5(rs.getInt("stock_size5"));
                    currentStockInfo.setSize6(rs.getInt("stock_size6"));
                    currentStockInfo.setSize7(rs.getInt("stock_size7"));
                    currentStockInfo.setSize8(rs.getInt("stock_size8"));
                    currentStockInfo.setSize9(rs.getInt("stock_size9"));
                    currentStockInfo.setSubQuantity(rs.getInt("stock_sub_quantity"));
                    productInfo.setCurrentStockInfo(currentStockInfo);
                    //      
                    colorInfo.setColorCode(rs.getString("color_code"));
                    colorInfo.setColorName(rs.getString("color_name"));
                    productInfo.setColorInfo(colorInfo);
                    //    
                    ageGroupInfo.setAgeCode(rs.getString("age_group_code"));
                    ageGroupInfo.setAgeCodeName(rs.getString("age_group_name"));
                    productInfo.setAgeGroupInfo(ageGroupInfo);
                    //            
                    genderGroupInfo.setGenderCode(rs.getString("gender_group_code"));
                    genderGroupInfo.setGenderCodeName(rs.getString("gender_group_name"));
                    productInfo.setGenderGroupInfo(genderGroupInfo);
                    //
                    productOrderDetailsInfo.setOrderDate(rs.getString("order_date"));
                    productInfo.setProductOrderDetailsInfo(productOrderDetailsInfo);
                    //
                    productInfoList.add(productInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productInfoList;
    }

    public ArrayList<InvoiceMasterInfo> getProductInvoiceInfo(Connection connection) {

        InvoiceMasterInfo invoiceMasterInfo = null;
        ArrayList<InvoiceMasterInfo> productInvoiceInfoList = null;
        //  
        InvoiceChildInfo invoiceChildInfo = null;
        ProductOrderInfo productOrderInfo = null;
        ProductOrderDetailsInfo productOrderDetailsInfo;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductInvoiceInfo());
                rs = ps.executeQuery();

                productInvoiceInfoList = new ArrayList<InvoiceMasterInfo>();

                while (rs.next()) {

                    invoiceMasterInfo = new InvoiceMasterInfo();
                    // 
                    invoiceChildInfo = new InvoiceChildInfo();
                    productOrderInfo = new ProductOrderInfo();
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    companyInfo = new CompanyInfo();
                    //   
                    invoiceMasterInfo.setGrandTotal(rs.getDouble("grand_total") == 0.0 ? 0.0 : rs.getDouble("grand_total"));
                    invoiceMasterInfo.setDiscount(rs.getDouble("discount") == 0.0 ? 0.0 : rs.getDouble("discount"));
                    invoiceMasterInfo.setDeliveryCharge(rs.getInt("delivery_charge") == 0 ? 0 : rs.getInt("delivery_charge"));
                    invoiceMasterInfo.setAdvance(rs.getInt("advance") == 0 ? 0 : rs.getInt("advance"));
                    invoiceMasterInfo.setInvoiceNo(rs.getString("invoice_no"));
                    invoiceMasterInfo.setInvoiceStatus(rs.getString("invoice_status").charAt(0));
                    invoiceMasterInfo.setInvoiceBy(rs.getString("invoice_by"));
                    invoiceMasterInfo.setCustomerName(rs.getString("customer_name"));
                    invoiceMasterInfo.setCustomerID(rs.getString("customer_id"));
                    invoiceMasterInfo.setCustomerMobile(rs.getString("customer_mobile"));
                    invoiceMasterInfo.setCustomerAddress(rs.getString("customer_address"));

                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date dbDate = rs.getTimestamp("invoice_date");
                    String fDate = formatter.format(dbDate);
                    invoiceMasterInfo.setInvoiceDate(fDate);

                    invoiceChildInfo.setItemCode(rs.getString("item_code"));
                    invoiceChildInfo.setProductName(rs.getString("product_name"));
                    invoiceChildInfo.setInvoiceQuantity(rs.getInt("invoice_quantity") == 0 ? 0 : rs.getInt("invoice_quantity"));
                    invoiceChildInfo.setInvoiceMRP(rs.getDouble("invoice_mrp") == 0.0 ? 0.0 : rs.getDouble("invoice_mrp"));
                    invoiceChildInfo.setInvoiceSubTotal(rs.getDouble("invoice_sub_total") == 0.0 ? 0.0 : rs.getDouble("invoice_sub_total"));
                    invoiceMasterInfo.setInvoiceChildInfo(invoiceChildInfo);

                    productInvoiceInfoList.add(invoiceMasterInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productInvoiceInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productInvoiceInfoList;
    }

    public ArrayList<InvoiceMasterInfo> getProductInvoiceInfoByInvNo(Connection connection, String invNo) {

        InvoiceMasterInfo invoiceMasterInfo = null;
        ArrayList<InvoiceMasterInfo> productInvoiceInfoList = null;
        //  
        InvoiceChildInfo invoiceChildInfo = null;
        ProductOrderInfo productOrderInfo = null;
        ProductOrderDetailsInfo productOrderDetailsInfo;
        CompanyInfo companyInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductInvoiceInfoByNo(invNo));
                rs = ps.executeQuery();

                productInvoiceInfoList = new ArrayList<InvoiceMasterInfo>();

                while (rs.next()) {

                    invoiceMasterInfo = new InvoiceMasterInfo();
                    // 
                    invoiceChildInfo = new InvoiceChildInfo();
                    productOrderInfo = new ProductOrderInfo();
                    productOrderDetailsInfo = new ProductOrderDetailsInfo();
                    companyInfo = new CompanyInfo();
                    //   
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                    Date dbDate = rs.getTimestamp("invoice_date");
                    String fDate = formatter.format(dbDate);
                    invoiceMasterInfo.setInvoiceDate(fDate);

                    invoiceMasterInfo.setInvoiceNo(rs.getString("invoice_no"));
                    invoiceMasterInfo.setInvoiceStatus(rs.getString("invoice_status").charAt(0));
                    invoiceMasterInfo.setInvoiceBy(rs.getString("invoice_by"));
                    invoiceMasterInfo.setCustomerName(rs.getString("customer_name"));
                    invoiceMasterInfo.setCustomerID(rs.getString("customer_id"));
                    invoiceMasterInfo.setCustomerMobile(rs.getString("customer_mobile"));
                    invoiceMasterInfo.setCustomerAddress(rs.getString("customer_address"));
                    //                    
                    invoiceChildInfo.setInvoiceChildID(rs.getInt("invoice_child_id"));
                    invoiceChildInfo.setItemCode(rs.getString("item_code"));
                    invoiceChildInfo.setInvoiceSize(rs.getString("invoice_size"));
                    invoiceChildInfo.setInvoiceSize0(rs.getInt("invoice_size0"));
                    invoiceChildInfo.setInvoiceSize1(rs.getInt("invoice_size1"));
                    invoiceChildInfo.setInvoiceSize2(rs.getInt("invoice_size2"));
                    invoiceChildInfo.setInvoiceSize3(rs.getInt("invoice_size3"));
                    invoiceChildInfo.setInvoiceSize4(rs.getInt("invoice_size4"));
                    invoiceChildInfo.setInvoiceSize5(rs.getInt("invoice_size5"));
                    invoiceChildInfo.setInvoiceSize6(rs.getInt("invoice_size6"));
                    invoiceChildInfo.setInvoiceSize7(rs.getInt("invoice_size7"));
                    invoiceChildInfo.setInvoiceSize8(rs.getInt("invoice_size8"));
                    invoiceChildInfo.setInvoiceSize9(rs.getInt("invoice_size9"));
                    invoiceChildInfo.setInvoiceQuantity(rs.getInt("invoice_quantity"));
                    invoiceChildInfo.setInvoiceMRP(rs.getDouble("invoice_mrp"));
                    invoiceChildInfo.setInvoiceSubTotal(rs.getDouble("invoice_sub_total"));
                    invoiceMasterInfo.setInvoiceChildInfo(invoiceChildInfo);

                    productInvoiceInfoList.add(invoiceMasterInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productInvoiceInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productInvoiceInfoList;
    }

    public ArrayList<CustomerInfo> getAllCustomer(Connection connection) {

        CustomerInfo customerInfo;
        ArrayList<CustomerInfo> customerInfoList = null;
        //        
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCustomerInfo());
                rs = ps.executeQuery();

                customerInfoList = new ArrayList<CustomerInfo>();

                while (rs.next()) {

                    customerInfo = new CustomerInfo();
                    //   
                    customerInfo.setCustomerID(rs.getString("customer_id"));
                    customerInfo.setContactNo(rs.getString("contact_no"));
                    customerInfo.setFullName(rs.getString("full_name"));
                    customerInfo.setDateOfBirth(rs.getString("dob"));
                    customerInfo.setReligion(rs.getString("religion"));
                    customerInfo.setGender(rs.getString("gender"));
                    customerInfo.setHotLine(rs.getString("hot_line"));
                    customerInfo.setAddress(rs.getString("address"));
                    customerInfo.setCustomerStatus(rs.getString("status").charAt(0));

                    customerInfoList.add(customerInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                customerInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return customerInfoList;
    }

    public ArrayList<ProductReturnInfo> getAllReturnedProduct(Connection connection) {

        ProductReturnInfo productReturnInfo = null;
        ArrayList<ProductReturnInfo> productReturnInfoList = null;
        //   
        InvoiceMasterInfo invoiceMasterInfo;
        //        
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectReturnedProductInfo());
                rs = ps.executeQuery();

                productReturnInfoList = new ArrayList<ProductReturnInfo>();

                while (rs.next()) {

                    productReturnInfo = new ProductReturnInfo();
                    //
                    invoiceMasterInfo = new InvoiceMasterInfo();
                    //   
                    productReturnInfo.setItemCode(rs.getString("item_code"));
                    productReturnInfo.setInvoiceNo(rs.getString("invoice_no"));
                    productReturnInfo.setSizeUnit(rs.getString("size_unit"));
                    productReturnInfo.setReturnSize0(rs.getInt("return_size0"));
                    productReturnInfo.setReturnSize1(rs.getInt("return_size1"));
                    productReturnInfo.setReturnSize2(rs.getInt("return_size2"));
                    productReturnInfo.setReturnSize3(rs.getInt("return_size3"));
                    productReturnInfo.setReturnSize4(rs.getInt("return_size4"));
                    productReturnInfo.setReturnSize5(rs.getInt("return_size5"));
                    productReturnInfo.setReturnSize6(rs.getInt("return_size6"));
                    productReturnInfo.setReturnSize7(rs.getInt("return_size7"));
                    productReturnInfo.setReturnSize8(rs.getInt("return_size8"));
                    productReturnInfo.setReturnSize9(rs.getInt("return_size9"));
                    productReturnInfo.setReturnQuantity(rs.getInt("return_quantity"));
                    productReturnInfo.setReturnCause(rs.getString("return_cause"));
                    productReturnInfo.setReturnStatus(rs.getString("return_status").charAt(0));

                    invoiceMasterInfo.setCustomerID(rs.getString("customer_id"));
                    invoiceMasterInfo.setCustomerName(rs.getString("customer_name"));
                    invoiceMasterInfo.setCustomerMobile(rs.getString("customer_mobile"));
                    invoiceMasterInfo.setCustomerAddress(rs.getString("customer_address"));
                    invoiceMasterInfo.setInvoiceDate(rs.getString("invoice_date"));
                    productReturnInfo.setInvoiceMasterInfo(invoiceMasterInfo);

                    productReturnInfoList.add(productReturnInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productReturnInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productReturnInfoList;
    }

    public ArrayList<ExchangeMasterInfo> getAllExchangedProduct(Connection connection) {

        ExchangeMasterInfo exchangeMasterInfo = null;
        ArrayList<ExchangeMasterInfo> exchangeMasterInfoList = null;
        //   
        ExchangeChildInfo exchangeChildInfo;
        //        
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectExchangedProductInfo());
                rs = ps.executeQuery();

                exchangeMasterInfoList = new ArrayList<ExchangeMasterInfo>();

                while (rs.next()) {

                    exchangeMasterInfo = new ExchangeMasterInfo();
                    //
                    exchangeChildInfo = new ExchangeChildInfo();
                    //  
                    exchangeMasterInfo.setInvoiceNo(rs.getString("invoice_no"));
                    exchangeMasterInfo.setCustomerID(rs.getString("customer_id"));
                    exchangeMasterInfo.setCustomerName(rs.getString("customer_name"));
                    exchangeMasterInfo.setCustomerMobile(rs.getString("customer_mobile"));
                    exchangeMasterInfo.setExchangeCause(rs.getString("exchange_cause"));
                    //                    
                    exchangeChildInfo.setOldItemCode(rs.getString("old_item_code"));
                    exchangeChildInfo.setOldSizeUnit(rs.getString("old_size_unit"));
                    exchangeChildInfo.setOldSize0(rs.getInt("old_size0"));
                    exchangeChildInfo.setOldSize1(rs.getInt("old_size1"));
                    exchangeChildInfo.setOldSize2(rs.getInt("old_size2"));
                    exchangeChildInfo.setOldSize3(rs.getInt("old_size3"));
                    exchangeChildInfo.setOldSize4(rs.getInt("old_size4"));
                    exchangeChildInfo.setOldSize5(rs.getInt("old_size5"));
                    exchangeChildInfo.setOldSize6(rs.getInt("old_size6"));
                    exchangeChildInfo.setOldSize7(rs.getInt("old_size7"));
                    exchangeChildInfo.setOldSize8(rs.getInt("old_size8"));
                    exchangeChildInfo.setOldSize9(rs.getInt("old_size9"));
                    exchangeChildInfo.setOldQuantity(rs.getInt("old_quantity"));

                    exchangeChildInfo.setNewItemCode(rs.getString("new_item_code"));
                    exchangeChildInfo.setNewSizeUnit(rs.getString("new_size_unit"));
                    exchangeChildInfo.setNewSize0(rs.getInt("new_size0"));
                    exchangeChildInfo.setNewSize1(rs.getInt("new_size1"));
                    exchangeChildInfo.setNewSize2(rs.getInt("new_size2"));
                    exchangeChildInfo.setNewSize3(rs.getInt("new_size3"));
                    exchangeChildInfo.setNewSize4(rs.getInt("new_size4"));
                    exchangeChildInfo.setNewSize5(rs.getInt("new_size5"));
                    exchangeChildInfo.setNewSize6(rs.getInt("new_size6"));
                    exchangeChildInfo.setNewSize7(rs.getInt("new_size7"));
                    exchangeChildInfo.setNewSize8(rs.getInt("new_size8"));
                    exchangeChildInfo.setNewSize9(rs.getInt("new_size9"));
                    exchangeChildInfo.setNewQuantity(rs.getInt("new_quantity"));

                    exchangeMasterInfo.setExchangeChild(exchangeChildInfo);
                    exchangeMasterInfoList.add(exchangeMasterInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                exchangeMasterInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return exchangeMasterInfoList;
    }

    public ArrayList<ProductInfo> getProductDetailsForUpdate(Connection connection, String itemCode) {

        ProductInfo productInfo = null;
        ArrayList<ProductInfo> productInfoList = null;
        //        
        BrandInfo brandInfo = null;
        SizeInfo sizeInfo = null;
        SubCategoryInfo subCategoryInfo = null;
        CategoryInfo categoryInfo = null;

        ColorInfo colorInfo = null;
        AgeGroupInfo ageGroupInfo = null;
        GenderGroupInfo genderGroupInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectProductDetailsForUpdate(itemCode));
                rs = ps.executeQuery();

                productInfoList = new ArrayList<ProductInfo>();

                while (rs.next()) {

                    productInfo = new ProductInfo();
                    //
                    sizeInfo = new SizeInfo();
                    brandInfo = new BrandInfo();
                    categoryInfo = new CategoryInfo();
                    subCategoryInfo = new SubCategoryInfo();
                    colorInfo = new ColorInfo();
                    ageGroupInfo = new AgeGroupInfo();
                    genderGroupInfo = new GenderGroupInfo();
                    //
                    productInfo.setItemCode(rs.getString("pro_item_code"));
                    productInfo.setProductName(rs.getString("pro_name"));
                    productInfo.setItemCostPrice(rs.getDouble("pro_cost_price"));
                    productInfo.setItemVat(rs.getDouble("pro_vat"));
                    productInfo.setItemNetPrice(rs.getDouble("pro_net_price"));
                    productInfo.setItemMRP(rs.getDouble("pro_mrp"));
                    productInfo.setItemLifeCycle(rs.getDate("pro_life_cycle"));
                    productInfo.setItemSupplier(rs.getString("pro_supplier"));
                    productInfo.setItemSuppReference(rs.getString("pro_supp_reference"));
                    productInfo.setItemState(rs.getString("pro_state"));
                    productInfo.setItemTemperature(rs.getString("pro_temp"));
                    productInfo.setItemDescription(rs.getString("pro_desc"));
                    productInfo.setItemImageURL(rs.getString("pro_image_url"));
                    //    
                    sizeInfo.setSizeCode(rs.getString("size_code").charAt(0));
                    sizeInfo.setSizeUnit(rs.getString("size_unit"));
                    productInfo.setSizeInfo(sizeInfo);
                    //                    
                    brandInfo.setBrandCode(rs.getString("brand_code"));
                    brandInfo.setBrandName(rs.getString("brand_name"));
                    productInfo.setBrandInfo(brandInfo);
                    //    
                    categoryInfo.setCategoryCode(rs.getString("category_code"));
                    categoryInfo.setCategoryName(rs.getString("category_name"));
                    productInfo.setCategoryInfo(categoryInfo);
                    //   
                    subCategoryInfo.setSubCategoryCode(rs.getString("sub_category_code"));
                    subCategoryInfo.setSubCategoryName(rs.getString("sub_category_name"));
                    productInfo.setSubCategoryInfo(subCategoryInfo);
                    //      
                    colorInfo.setColorCode(rs.getString("color_code"));
                    colorInfo.setColorName(rs.getString("color_name"));
                    productInfo.setColorInfo(colorInfo);
                    //    
                    ageGroupInfo.setAgeCode(rs.getString("age_group_code"));
                    ageGroupInfo.setAgeCodeName(rs.getString("age_group_name"));
                    productInfo.setAgeGroupInfo(ageGroupInfo);
                    //            
                    genderGroupInfo.setGenderCode(rs.getString("gender_group_code"));
                    genderGroupInfo.setGenderCodeName(rs.getString("gender_group_name"));
                    productInfo.setGenderGroupInfo(genderGroupInfo);
                    //
                    productInfoList.add(productInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                productInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return productInfoList;
    }

    public static void main(String[] args) {
        Connection c = DBConnection.getMySqlConnection();
        CommonList com = new CommonList();
        System.out.println(com.getProductDetailsForInvoice(c, "Davmmmocbc0004"));
    }
}
