package com.common;

import com.common.DBConnection;
import java.awt.Container;
import static java.awt.Frame.MAXIMIZED_BOTH;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JRViewer;

public class InvoiceReport extends JFrame {

    private HashMap session = null;
    private Connection connection = null;
    private String reportName = "";

    public InvoiceReport() {

        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public InvoiceReport(HashMap s) {

        this.session = s;
        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public InvoiceReport(HashMap s, Connection connection) {

        this.session = s;
        this.connection = connection;
        setExtendedState(MAXIMIZED_BOTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("Invoice Report");
    }

    public void setReportName(String rptName) {
        this.reportName = rptName;
    }

    public void callReport() {

        JasperPrint jasperPrint = generateReport();

        JRViewer viewer = new JRViewer(jasperPrint);
        Container c = getContentPane();
        c.add(viewer);
        this.setVisible(true);
    }

    public JasperPrint generateReport() {

        JasperReport jasperReport = null;
        JasperPrint jasperPrint = null;
        InputStream path;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (session == null) {
            session = new HashMap();
        }

        if (connection != null) {

            try {

                path = this.getClass().getResourceAsStream("/report/" + reportName + ".jrxml");

                jasperReport = JasperCompileManager.compileReport(path);

                jasperPrint = JasperFillManager.fillReport(jasperReport, session, connection);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return jasperPrint;
    }
}
