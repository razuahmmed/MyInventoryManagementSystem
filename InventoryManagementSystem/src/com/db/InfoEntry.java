package com.db;

import com.common.DBConnection;
import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InfoEntry {

    public int passwordUpdate(Connection connection, String email, String pass, String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updatePasswordQuery(email, pass, updateBy));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveSizeInfo(
            Connection connection,
            Character sizeCode,
            String sizeUnit,
            String size0,
            String size1,
            String size2,
            String size3,
            String size4,
            String size5,
            String size6,
            String size7,
            String size8,
            String size9,
            String sizeDesc,
            String comId,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " size_info "
                    + " (size_code, "
                    + " size_unit, "
                    + " size0, "
                    + " size1, "
                    + " size2, "
                    + " size3, "
                    + " size4, "
                    + " size5, "
                    + " size6, "
                    + " size7, "
                    + " size8, "
                    + " size9, "
                    + " size_desc, "
                    + " company_id, "
                    + " insert_by, "
                    + " insert_date)"
                    + " VALUES("
                    + " '" + sizeCode + "' , "
                    + " '" + sizeUnit + "' , "
                    + " '" + size0 + "', "
                    + " '" + size1 + "' , "
                    + " '" + size2 + "' , "
                    + " '" + size3 + "' , "
                    + " '" + size4 + "' , "
                    + " '" + size5 + "' , "
                    + " '" + size6 + "' , "
                    + " '" + size7 + "' , "
                    + " '" + size8 + "' , "
                    + " '" + size9 + "' , "
                    + " '" + sizeDesc + "' , "
                    + " '" + comId + "' , "
                    + " '" + insertBy + "' , "
                    + " CURRENT_TIMESTAMP )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int editSizeInfo(
            Connection connection,
            Character sizeCode,
            String sizeUnit,
            String size0,
            String size1,
            String size2,
            String size3,
            String size4,
            String size5,
            String size6,
            String size7,
            String size8,
            String size9,
            String sizeDesc,
            String comId,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " size_info "
                    + " SET "
                    + " size_unit = '" + sizeUnit + "', "
                    + " size0 = '" + size0 + "', "
                    + " size1 = '" + size1 + "', "
                    + " size2 = '" + size2 + "', "
                    + " size3 = '" + size3 + "', "
                    + " size4 = '" + size4 + "', "
                    + " size5 = '" + size5 + "', "
                    + " size6 = '" + size6 + "', "
                    + " size7 = '" + size7 + "', "
                    + " size8 = '" + size8 + "', "
                    + " size9 = '" + size9 + "', "
                    + " size_desc = '" + sizeDesc + "', "
                    + " company_id = '" + comId + "', "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " size_code = '" + sizeCode + "'";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteSizeInfo(
            Connection connection,
            Character sizeCode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " size_info "
                    + " WHERE "
                    + " size_code = '" + sizeCode + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveBrandInfo(
            Connection connection,
            String brandCode,
            String brandName,
            String brandDesc,
            String comId,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " brand_info "
                    + " (brand_code, "
                    + " brand_name, "
                    + " brand_description, "
                    + " company_id, "
                    + " insert_by, "
                    + " insert_date)"
                    + " VALUES("
                    + " '" + brandCode + "', "
                    + " '" + brandName + "', "
                    + " '" + brandDesc + "', "
                    + " '" + comId + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateBrandInfo(
            Connection connection,
            String brandCode,
            String brandName,
            String brandDesc,
            String comId,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " brand_info "
                    + " SET "
                    + " brand_name = '" + brandName + "', "
                    + " brand_description = '" + brandDesc + "', "
                    + " company_id = '" + comId + "', "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " brand_code = '" + brandCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteBrandInfo(
            Connection connection,
            String brandCode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " brand_info "
                    + " WHERE "
                    + " brand_code = '" + brandCode + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveCategoryInfo(
            Connection connection,
            String cCode,
            String cName,
            String cDesc,
            String comId,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " category_info "
                    + " (category_code, "
                    + " category_name, "
                    + " category_description, "
                    + " company_id, "
                    + " insert_by, "
                    + " insert_date)"
                    + " VALUES("
                    + " '" + cCode + "', "
                    + " '" + cName + "', "
                    + " '" + cDesc + "', "
                    + " '" + comId + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int editCategoryInfo(
            Connection connection,
            String cCode,
            String cName,
            String cDesc,
            String comId,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " category_info "
                    + " SET "
                    + " category_name = '" + cName + "', "
                    + " category_description = '" + cDesc + "', "
                    + " company_id = '" + comId + "', "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " category_code = '" + cCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteCategoryInfo(
            Connection connection,
            String cCode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " category_info "
                    + " WHERE "
                    + " category_code = '" + cCode + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveSubCategoryInfo(
            Connection connection,
            String subCCode,
            String subCName,
            String subCDesc,
            String insertBy,
            String cCode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " sub_category_info "
                    + " (sub_category_code, "
                    + " sub_category_name, "
                    + " sub_category_description, "
                    + " insert_by, "
                    + " insert_date, "
                    + " category_code, "
                    + " company_id "
                    + " )"
                    + " VALUES("
                    + " '" + subCCode + "', "
                    + " '" + subCName + "', "
                    + " '" + subCDesc + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP, "
                    + " '" + cCode + "', "
                    + " (SELECT com_id FROM company_info) )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int editSubCategoryInfo(
            Connection connection,
            String subCCode,
            String subCName,
            String subCDesc,
            String updateBy,
            String cCode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " sub_category_info "
                    + " SET "
                    + " sub_category_name = '" + subCName + "', "
                    + " sub_category_description = '" + subCDesc + "', "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP, "
                    + " category_code = '" + cCode + "' "
                    + " WHERE "
                    + " sub_category_code = '" + subCCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteSubCategoryInfo(
            Connection connection,
            String subCCode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " sub_category_info "
                    + " WHERE "
                    + " sub_category_code = '" + subCCode + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveProduct(
            Connection connection,
            String pICode,
            String pName,
            Double pCPrice,
            Double pVat,
            Double pNPrice,
            Double pMrp,
            String pSupplier,
            String pSReference,
            String pState,
            String pTemp,
            String pLCycle,
            String pDesc,
            String pIUrl,
            String peCatCode,
            String peSubCatCode,
            String peSize,
            String peColor,
            String peBrand,
            String uGenGroup,
            String uAgeGroup,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " product_info( "
                    + " pro_item_code, "
                    + " pro_name, "
                    + " pro_cost_price, "
                    + " pro_vat, "
                    + " pro_net_price, "
                    + " pro_mrp, "
                    + " pro_supplier, "
                    + " pro_supp_reference, "
                    + " pro_state, "
                    + " pro_temp, "
                    + " pro_life_cycle, "
                    + " pro_desc, "
                    + " pro_image_url, "
                    + " category_code, "
                    + " sub_category_code, "
                    + " size_code, "
                    + " color_code, "
                    + " brand_code, "
                    + " gender_group_code, "
                    + " age_group_code, "
                    + " company_id, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + pICode + "', "
                    + " '" + pName + "', "
                    + " '" + pCPrice + "', "
                    + " '" + pVat + "', "
                    + " '" + pNPrice + "', "
                    + " '" + pMrp + "', "
                    + " '" + pSupplier + "', "
                    + " '" + pSReference + "', "
                    + " '" + pState + "', "
                    + " '" + pTemp + "', "
                    + " '" + pLCycle + "', "
                    + " '" + pDesc + "', "
                    + " '" + pIUrl + "', "
                    + " '" + peCatCode + "', "
                    + " '" + peSubCatCode + "', "
                    + " '" + peSize + "', "
                    + " '" + peColor + "', "
                    + " '" + peBrand + "', "
                    + " '" + uGenGroup + "', "
                    + " '" + uAgeGroup + "', "
                    + " (SELECT com_id FROM company_info), "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int editProduct(
            Connection connection,
            String productName,
            String pICode,
            Double pCPrice,
            Double pVat,
            Double pNPrice,
            Double pMrp,
            String pSupplier,
            String pSReference,
            String pState,
            String pTemp,
            String pLCycle,
            String pDesc,
            String pIUrl,
            String peCatCode,
            String peSubCatCode,
            String peSize,
            String peColor,
            String peBrand,
            String uGenGroup,
            String uAgeGroup,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " product_info "
                    + " SET "
                    + " pro_name = '" + productName + "', "
                    + " pro_cost_price = '" + pCPrice + "', "
                    + " pro_vat = '" + pVat + "', "
                    + " pro_net_price = '" + pNPrice + "', "
                    + " pro_mrp = '" + pMrp + "', "
                    + " pro_supplier = '" + pSupplier + "', "
                    + " pro_supp_reference = '" + pSReference + "', "
                    + " pro_state = '" + pState + "', "
                    + " pro_temp = '" + pTemp + "', "
                    + " pro_life_cycle = '" + pLCycle + "', "
                    + " pro_desc = '" + pDesc + "', "
                    + " pro_image_url = '" + pIUrl + "', "
                    + " size_code = '" + peSize + "', "
                    + " brand_code = '" + peBrand + "', "
                    + " category_code = '" + peCatCode + "', "
                    + " sub_category_code = '" + peSubCatCode + "', "
                    + " color_code = '" + peColor + "', "
                    + " gender_group_code = '" + uGenGroup + "', "
                    + " age_group_code = '" + uAgeGroup + "', "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " pro_item_code = '" + pICode + "' ";

            try {
                ps = connection.prepareStatement(updateQueryStatement);
                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteProduct(
            Connection connection,
            String pICode) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " product_info "
                    + " WHERE "
                    + " pro_item_code = '" + pICode + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveProductOrder(
            Connection connection,
            String pPurchaseOrderNo,
            String pPurchaseOrderDate,
            String poSupplierId,
            String poSupplierName,
            String poSupplierCompany,
            String poSupplierPhone,
            String poSupplierFax,
            String poSupplierEmail,
            String poSupplierZipCode,
            String poSupplierAddress,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " product_order_info( "
                    + " product_order_no, "
                    + " purchase_order_date, "
                    + " supplier_id, "
                    + " supplier_name, "
                    + " supplier_company, "
                    + " supplier_phone, "
                    + " supplier_fax, "
                    + " supplier_email, "
                    + " supplier_zip_code, "
                    + " supplier_address, "
                    + " company_id, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + pPurchaseOrderNo + "', "
                    + " '" + pPurchaseOrderDate + "', "
                    + " '" + poSupplierId + "', "
                    + " '" + poSupplierName + "', "
                    + " '" + poSupplierCompany + "', "
                    + " '" + poSupplierPhone + "', "
                    + " '" + poSupplierFax + "', "
                    + " '" + poSupplierEmail + "', "
                    + " '" + poSupplierZipCode + "', "
                    + " '" + poSupplierAddress + "', "
                    + " (SELECT com_id FROM company_info), "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteProductOrderAndOrderDetails(
            Connection connection,
            String orderNo) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE "
                    + " product_order_info POI, "
                    + " product_order_details POD "
                    + " FROM "
                    + " product_order_info POI "
                    + " INNER JOIN "
                    + " product_order_details POD "
                    + " ON "
                    + " POI.product_order_no = POD.product_order_no "
                    + " WHERE "
                    + " POI.product_order_no = '" + orderNo + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveProductOrderDetails(
            Connection connection,
            String iCode,
            Character sizeCode,
            Double netPrice,
            Integer quantity,
            Integer size0,
            Integer size1,
            Integer size2,
            Integer size3,
            Integer size4,
            Integer size5,
            Integer size6,
            Integer size7,
            Integer size8,
            Integer size9,
            Double subTotal,
            String pPurchaseOrderNo,
            String orderDate,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " product_order_details( "
                    + " item_code, "
                    + " size_code, "
                    + " net_price, "
                    + " quantity, "
                    + " s0, "
                    + " s1, "
                    + " s2, "
                    + " s3, "
                    + " s4, "
                    + " s5, "
                    + " s6, "
                    + " s7, "
                    + " s8, "
                    + " s9, "
                    + " sub_total, "
                    + " product_order_no, "
                    + " order_date, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + iCode + "', "
                    + " '" + sizeCode + "', "
                    + " '" + netPrice + "', "
                    + " '" + quantity + "', "
                    + " '" + size0 + "', "
                    + " '" + size1 + "', "
                    + " '" + size2 + "', "
                    + " '" + size3 + "', "
                    + " '" + size4 + "', "
                    + " '" + size5 + "', "
                    + " '" + size6 + "', "
                    + " '" + size7 + "', "
                    + " '" + size8 + "', "
                    + " '" + size9 + "', "
                    + " '" + subTotal + "', "
                    + " '" + pPurchaseOrderNo + "', "
                    + " '" + orderDate + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP )";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveProductOrderReceive(
            Connection connection,
            String orderNo,
            String orderDate,
            String itemCode,
            Character sizeCode,
            Integer size0,
            Integer size1,
            Integer size2,
            Integer size3,
            Integer size4,
            Integer size5,
            Integer size6,
            Integer size7,
            Integer size8,
            Integer size9,
            Double mrp,
            Integer quantity,
            Double subTotal,
            String receiveBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

//            String insertQueryStatement = " INSERT INTO "
//                    + " order_receive_info( "
//                    + " order_no, "
//                    + " order_date, "
//                    + " item_code, "
//                    + " size_code, "
//                    + " s0_received, "
//                    + " s1_received, "
//                    + " s2_received, "
//                    + " s3_received, "
//                    + " s4_received, "
//                    + " s5_received, "
//                    + " s6_received, "
//                    + " s7_received, "
//                    + " s8_received, "
//                    + " s9_received, "
//                    + " receive_mrp, "
//                    + " receive_quantity, "
//                    + " receive_sub_total, "
//                    + " received_by, "
//                    + " receive_date "
//                    + " )VALUES("
//                    + " '" + orderNo + "', "
//                    + " '" + orderDate + "', "
//                    + " '" + itemCode + "', "
//                    + " '" + sizeCode + "', "
//                    + " '" + size0 + "', "
//                    + " '" + size1 + "', "
//                    + " '" + size2 + "', "
//                    + " '" + size3 + "', "
//                    + " '" + size4 + "', "
//                    + " '" + size5 + "', "
//                    + " '" + size6 + "', "
//                    + " '" + size7 + "', "
//                    + " '" + size8 + "', "
//                    + " '" + size9 + "', "
//                    + " '" + mrp + "', "
//                    + " '" + quantity + "', "
//                    + " '" + subTotal + "', "
//                    + " '" + receiveBy + "', "
//                    + " CURRENT_TIMESTAMP )";
//            
//            String insertQueryStatement1 = " SELECT CASE WHEN EXISTS (SELECT * FROM order_receive_info WHERE order_no = " + orderNo + ") "
//                    + " THEN "
//                    + " UPDATE order_receive_info SET "
//                    + " s0_received = s0_received +" + size0 + ", "
//                    + " s1_received = s1_received +" + size1 + ", "
//                    + " s2_received = s2_received +" + size2 + ", "
//                    + " s3_received = s3_received +" + size3 + ", "
//                    + " s4_received = s4_received +" + size4 + ", "
//                    + " s5_received = s5_received +" + size5 + ", "
//                    + " s6_received = s6_received +" + size6 + ", "
//                    + " s7_received = s7_received +" + size7 + ", "
//                    + " s8_received = s8_received +" + size8 + ", "
//                    + " s9_received = s9_received +" + size9 + ", "
//                    + " receive_quantity = receive_quantity +" + quantity + ", "
//                    + " receive_sub_total = receive_sub_total +" + subTotal + ", "
//                    + " received_by = '" + receiveBy + "', "
//                    + " receive_date = CURRENT_TIMESTAMP "
//                    + " WHERE "
//                    + " order_no = " + orderNo + " "
//                    + " ELSE "
//                    + " INSERT INTO "
//                    + " order_receive_info( "
//                    + " order_no, "
//                    + " order_date, "
//                    + " item_code, "
//                    + " size_code, "
//                    + " s0_received, "
//                    + " s1_received, "
//                    + " s2_received, "
//                    + " s3_received, "
//                    + " s4_received, "
//                    + " s5_received, "
//                    + " s6_received, "
//                    + " s7_received, "
//                    + " s8_received, "
//                    + " s9_received, "
//                    + " receive_mrp, "
//                    + " receive_quantity, "
//                    + " receive_sub_total, "
//                    + " received_by, "
//                    + " receive_date "
//                    + " )VALUES("
//                    + " '" + orderNo + "', "
//                    + " '" + orderDate + "', "
//                    + " '" + itemCode + "', "
//                    + " '" + sizeCode + "', "
//                    + " '" + size0 + "', "
//                    + " '" + size1 + "', "
//                    + " '" + size2 + "', "
//                    + " '" + size3 + "', "
//                    + " '" + size4 + "', "
//                    + " '" + size5 + "', "
//                    + " '" + size6 + "', "
//                    + " '" + size7 + "', "
//                    + " '" + size8 + "', "
//                    + " '" + size9 + "', "
//                    + " '" + mrp + "', "
//                    + " '" + quantity + "', "
//                    + " '" + subTotal + "', "
//                    + " '" + receiveBy + "', "
//                    + " CURRENT_TIMESTAMP) END";
//            
            String insertUpdateQueryStatement = " INSERT INTO "
                    + " order_receive_info( "
                    + " order_no, "
                    + " order_date, "
                    + " item_code, "
                    + " size_code, "
                    + " s0_received, "
                    + " s1_received, "
                    + " s2_received, "
                    + " s3_received, "
                    + " s4_received, "
                    + " s5_received, "
                    + " s6_received, "
                    + " s7_received, "
                    + " s8_received, "
                    + " s9_received, "
                    + " receive_mrp, "
                    + " receive_quantity, "
                    + " receive_sub_total, "
                    + " received_by, "
                    + " receive_date "
                    + " )VALUES("
                    + " '" + orderNo + "', "
                    + " '" + orderDate + "', "
                    + " '" + itemCode + "', "
                    + " '" + sizeCode + "', "
                    + " '" + size0 + "', "
                    + " '" + size1 + "', "
                    + " '" + size2 + "', "
                    + " '" + size3 + "', "
                    + " '" + size4 + "', "
                    + " '" + size5 + "', "
                    + " '" + size6 + "', "
                    + " '" + size7 + "', "
                    + " '" + size8 + "', "
                    + " '" + size9 + "', "
                    + " '" + mrp + "', "
                    + " '" + quantity + "', "
                    + " '" + subTotal + "', "
                    + " '" + receiveBy + "', "
                    + " CURRENT_TIMESTAMP)"
                    + " ON DUPLICATE KEY UPDATE "
                    + " s0_received = s0_received+" + size0 + ", "
                    + " s1_received = s1_received+" + size1 + ", "
                    + " s2_received = s2_received+" + size2 + ", "
                    + " s3_received = s3_received+" + size3 + ", "
                    + " s4_received = s4_received+" + size4 + ", "
                    + " s5_received = s5_received+" + size5 + ", "
                    + " s6_received = s6_received+" + size6 + ", "
                    + " s7_received = s7_received+" + size7 + ", "
                    + " s8_received = s8_received+" + size8 + ", "
                    + " s9_received = s9_received+" + size9 + ", "
                    + " receive_quantity = receive_quantity +" + quantity + ", "
                    + " receive_sub_total = receive_sub_total +" + subTotal + ", "
                    + " received_by = '" + receiveBy + "', "
                    + " receive_date = CURRENT_TIMESTAMP";

            try {

                ps = connection.prepareStatement(insertUpdateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveProductStock(
            Connection connection,
            String itemCode,
            String orderNo,
            String sizeCode,
            Integer size0,
            Integer size1,
            Integer size2,
            Integer size3,
            Integer size4,
            Integer size5,
            Integer size6,
            Integer size7,
            Integer size8,
            Integer size9,
            Integer subQuantity,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " current_stock_info( "
                    + " item_code, "
                    + " order_no, "
                    + " size_code, "
                    + " stock_size0, "
                    + " stock_size1, "
                    + " stock_size2, "
                    + " stock_size3, "
                    + " stock_size4, "
                    + " stock_size5, "
                    + " stock_size6, "
                    + " stock_size7, "
                    + " stock_size8, "
                    + " stock_size9, "
                    + " stock_sub_quantity, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + itemCode + "', "
                    + " '" + orderNo + "', "
                    + " '" + sizeCode + "', "
                    + " '" + size0 + "', "
                    + " '" + size1 + "', "
                    + " '" + size2 + "', "
                    + " '" + size3 + "', "
                    + " '" + size4 + "', "
                    + " '" + size5 + "', "
                    + " '" + size6 + "', "
                    + " '" + size7 + "', "
                    + " '" + size8 + "', "
                    + " '" + size9 + "', "
                    + " '" + subQuantity + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP)"
                    + " ON DUPLICATE KEY UPDATE "
                    + " stock_size0 = stock_size0+" + size0 + ", "
                    + " stock_size1 = stock_size1+" + size1 + ", "
                    + " stock_size2 = stock_size2+" + size2 + ", "
                    + " stock_size3 = stock_size3+" + size3 + ", "
                    + " stock_size4 = stock_size4+" + size4 + ", "
                    + " stock_size5 = stock_size5+" + size5 + ", "
                    + " stock_size6 = stock_size6+" + size6 + ", "
                    + " stock_size7 = stock_size7+" + size7 + ", "
                    + " stock_size8 = stock_size8+" + size8 + ", "
                    + " stock_size9 = stock_size9+" + size9 + ", "
                    + " stock_sub_quantity = stock_sub_quantity+" + subQuantity + ", "
                    + " update_by = '" + insertBy + "', "
                    + " update_date = CURRENT_TIMESTAMP";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteReceiveOrder(
            Connection connection,
            String orderNo) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " order_receive_info "
                    + " WHERE "
                    + " order_no = '" + orderNo + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveCustomerInfo(
            Connection connection,
            String fName,
            String lName,
            String contctNo,
            String hotLineAddress,
            String address,
            String cusDOB,
            String religion,
            String gender,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String fullName = fName + " " + lName;

            String insertQueryStatement = " INSERT INTO "
                    + " customer_info( "
                    + " customer_id, "
                    + " fname, "
                    + " lname, "
                    + " full_name, "
                    + " contact_no, "
                    + " dob, "
                    + " religion, "
                    + " gender, "
                    + " hot_line, "
                    + " address, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + contctNo + "', "
                    + " '" + fName + "', "
                    + " '" + lName + "', "
                    + " '" + fullName + "', "
                    + " '" + contctNo + "', "
                    + " '" + cusDOB + "', "
                    + " '" + religion + "', "
                    + " '" + gender + "', "
                    + " '" + hotLineAddress + "', "
                    + " '" + address + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP)";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteCustomer(
            Connection connection,
            String customerID) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " customer_info "
                    + " WHERE "
                    + " customer_id = '" + customerID + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveInvoiceMaster(
            Connection connection,
            String invoiceNo,
            String customerID,
            String customerName,
            String customerMobile,
            String customerAddress,
            Double grandTotal,
            Double discount,
            Integer advance,
            Integer deliveryCharge,
            String invoiceBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " invoice_master( "
                    + " invoice_no, "
                    + " customer_id, "
                    + " customer_name, "
                    + " customer_mobile, "
                    + " customer_address, "
                    + " grand_total, "
                    + " discount, "
                    + " advance, "
                    + " delivery_charge, "
                    + " invoice_by, "
                    + " invoice_date "
                    + " )VALUES("
                    + " '" + invoiceNo + "', "
                    + " '" + customerID + "', "
                    + " '" + customerName + "', "
                    + " '" + customerMobile + "', "
                    + " '" + customerAddress + "', "
                    + " '" + grandTotal + "', "
                    + " '" + discount + "', "
                    + " '" + advance + "', "
                    + " '" + deliveryCharge + "', "
                    + " '" + invoiceBy + "', "
                    + " CURRENT_TIMESTAMP)";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveInvoiceChild(
            Connection connection,
            String itemCode,
            String invoiceSize,
            Integer invoiceSize0,
            Integer invoiceSize1,
            Integer invoiceSize2,
            Integer invoiceSize3,
            Integer invoiceSize4,
            Integer invoiceSize5,
            Integer invoiceSize6,
            Integer invoiceSize7,
            Integer invoiceSize8,
            Integer invoiceSize9,
            Double invoiceMrp,
            Integer invoiceQuantity,
            Double invoiceSubTotal,
            String invoiceNo,
            String pName) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " invoice_child( "
                    + " item_code, "
                    + " invoice_size, "
                    + " invoice_size0, "
                    + " invoice_size1, "
                    + " invoice_size2, "
                    + " invoice_size3, "
                    + " invoice_size4, "
                    + " invoice_size5, "
                    + " invoice_size6, "
                    + " invoice_size7, "
                    + " invoice_size8, "
                    + " invoice_size9, "
                    + " invoice_mrp, "
                    + " invoice_quantity, "
                    + " invoice_sub_total, "
                    + " invoice_no, "
                    + " product_name "
                    + " )VALUES("
                    + " '" + itemCode + "', "
                    + " '" + invoiceSize + "', "
                    + " '" + invoiceSize0 + "', "
                    + " '" + invoiceSize1 + "', "
                    + " '" + invoiceSize2 + "', "
                    + " '" + invoiceSize3 + "', "
                    + " '" + invoiceSize4 + "', "
                    + " '" + invoiceSize5 + "', "
                    + " '" + invoiceSize6 + "', "
                    + " '" + invoiceSize7 + "', "
                    + " '" + invoiceSize8 + "', "
                    + " '" + invoiceSize9 + "', "
                    + " '" + invoiceMrp + "', "
                    + " '" + invoiceQuantity + "', "
                    + " '" + invoiceSubTotal + "', "
                    + " '" + invoiceNo + "', "
                    + " '" + pName + "')";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateProductStock(
            Connection connection,
            String itemCode,
            Integer invoiceSize0,
            Integer invoiceSize1,
            Integer invoiceSize2,
            Integer invoiceSize3,
            Integer invoiceSize4,
            Integer invoiceSize5,
            Integer invoiceSize6,
            Integer invoiceSize7,
            Integer invoiceSize8,
            Integer invoiceSize9,
            Integer invoiceQuantity,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " current_stock_info "
                    + " SET "
                    + " stock_size0 = stock_size0-" + invoiceSize0 + ", "
                    + " stock_size1 = stock_size1-" + invoiceSize1 + ", "
                    + " stock_size2 = stock_size2-" + invoiceSize2 + ", "
                    + " stock_size3 = stock_size3-" + invoiceSize3 + ", "
                    + " stock_size4 = stock_size4-" + invoiceSize4 + ", "
                    + " stock_size5 = stock_size5-" + invoiceSize5 + ", "
                    + " stock_size6 = stock_size6-" + invoiceSize6 + ", "
                    + " stock_size7 = stock_size7-" + invoiceSize7 + ", "
                    + " stock_size8 = stock_size8-" + invoiceSize8 + ", "
                    + " stock_size9 = stock_size9-" + invoiceSize9 + ", "
                    + " stock_sub_quantity = stock_sub_quantity-" + invoiceQuantity + ", "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " item_code = '" + itemCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveInvoiceReturnMaster(
            Connection connection,
            String invoiceNo,
            String customerName,
            String customerMobile,
            String customerID,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " return_master( "
                    + " invoice_no, "
                    + " customer_name, "
                    + " customer_mobile, "
                    + " customer_id, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + invoiceNo + "', "
                    + " '" + customerName + "', "
                    + " '" + customerMobile + "', "
                    + " '" + customerID + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP)";

            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveInvoiceReturnChild(
            Connection connection,
            String itemCode,
            String sizeUnit,
            Integer returnSize0,
            Integer returnSize1,
            Integer returnSize2,
            Integer returnSize3,
            Integer returnSize4,
            Integer returnSize5,
            Integer returnSize6,
            Integer returnSize7,
            Integer returnSize8,
            Integer returnSize9,
            Integer returnQuantity,
            String returnCause,
            String invoiceNo) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertQueryStatement = " INSERT INTO "
                    + " return_child( "
                    + " return_item_code, "
                    + " return_size_unit, "
                    + " return_size0, "
                    + " return_size1, "
                    + " return_size2, "
                    + " return_size3, "
                    + " return_size4, "
                    + " return_size5, "
                    + " return_size6, "
                    + " return_size7, "
                    + " return_size8, "
                    + " return_size9, "
                    + " return_quantity, "
                    + " return_cause, "
                    + " invoice_no "
                    + " )VALUES("
                    + " '" + itemCode + "', "
                    + " '" + sizeUnit + "', "
                    + " '" + returnSize0 + "', "
                    + " '" + returnSize1 + "', "
                    + " '" + returnSize2 + "', "
                    + " '" + returnSize3 + "', "
                    + " '" + returnSize4 + "', "
                    + " '" + returnSize5 + "', "
                    + " '" + returnSize6 + "', "
                    + " '" + returnSize7 + "', "
                    + " '" + returnSize8 + "', "
                    + " '" + returnSize9 + "', "
                    + " '" + returnQuantity + "', "
                    + " '" + returnCause + "', "
                    + " '" + invoiceNo + "')";

//                    + " ON DUPLICATE KEY UPDATE "
//                    + " stock_size0 = stock_size0+" + size0 + ", "
//                    + " stock_size1 = stock_size1+" + size1 + ", "
//                    + " stock_size2 = stock_size2+" + size2 + ", "
//                    + " stock_size3 = stock_size3+" + size3 + ", "
//                    + " stock_size4 = stock_size4+" + size4 + ", "
//                    + " stock_size5 = stock_size5+" + size5 + ", "
//                    + " stock_size6 = stock_size6+" + size6 + ", "
//                    + " stock_size7 = stock_size7+" + size7 + ", "
//                    + " stock_size8 = stock_size8+" + size8 + ", "
//                    + " stock_size9 = stock_size9+" + size9 + ", "
//                    + " stock_sub_quantity = stock_sub_quantity+" + subQuantity + ", "
//                    + " update_by = '" + insertBy + "', "
//                    + " update_date = CURRENT_TIMESTAMP";
            try {

                ps = connection.prepareStatement(insertQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateInvoiceForReturn(
            Connection connection,
            String itemCode,
            String invoice,
            Integer invoiceSize0,
            Integer invoiceSize1,
            Integer invoiceSize2,
            Integer invoiceSize3,
            Integer invoiceSize4,
            Integer invoiceSize5,
            Integer invoiceSize6,
            Integer invoiceSize7,
            Integer invoiceSize8,
            Integer invoiceSize9,
            Integer invoiceQuantity,
            Double subTotal,
            Double grandTotal,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " invoice_master IVM, "
                    + " invoice_child IVC "
                    + " SET "
                    + " IVM.grand_total = grand_total-" + grandTotal + ", "
                    + " IVM.update_by = '" + updateBy + "', "
                    + " IVM.update_date = CURRENT_TIMESTAMP, "
                    + " IVC.invoice_size0 = IVC.invoice_size0-" + invoiceSize0 + ", "
                    + " IVC.invoice_size1 = IVC.invoice_size1-" + invoiceSize1 + ", "
                    + " IVC.invoice_size2 = IVC.invoice_size2-" + invoiceSize2 + ", "
                    + " IVC.invoice_size3 = IVC.invoice_size3-" + invoiceSize3 + ", "
                    + " IVC.invoice_size4 = IVC.invoice_size4-" + invoiceSize4 + ", "
                    + " IVC.invoice_size5 = IVC.invoice_size5-" + invoiceSize5 + ", "
                    + " IVC.invoice_size6 = IVC.invoice_size6-" + invoiceSize6 + ", "
                    + " IVC.invoice_size7 = IVC.invoice_size7-" + invoiceSize7 + ", "
                    + " IVC.invoice_size8 = IVC.invoice_size8-" + invoiceSize8 + ", "
                    + " IVC.invoice_size9 = IVC.invoice_size9-" + invoiceSize9 + ", "
                    + " IVC.invoice_quantity = IVC.invoice_quantity-" + invoiceQuantity + ", "
                    + " IVC.invoice_sub_total = IVC.invoice_sub_total-" + subTotal + " "
                    + " WHERE "
                    + " IVM.invoice_no = IVC.invoice_no "
                    + " AND "
                    + " IVM.invoice_no = '" + invoice + "' "
                    + " AND "
                    + " IVC.item_code = '" + itemCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateProductStockForReturn(
            Connection connection,
            String itemCode,
            Integer invoiceSize0,
            Integer invoiceSize1,
            Integer invoiceSize2,
            Integer invoiceSize3,
            Integer invoiceSize4,
            Integer invoiceSize5,
            Integer invoiceSize6,
            Integer invoiceSize7,
            Integer invoiceSize8,
            Integer invoiceSize9,
            Integer invoiceQuantity,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " current_stock_info "
                    + " SET "
                    + " stock_size0 = stock_size0+" + invoiceSize0 + ", "
                    + " stock_size1 = stock_size1+" + invoiceSize1 + ", "
                    + " stock_size2 = stock_size2+" + invoiceSize2 + ", "
                    + " stock_size3 = stock_size3+" + invoiceSize3 + ", "
                    + " stock_size4 = stock_size4+" + invoiceSize4 + ", "
                    + " stock_size5 = stock_size5+" + invoiceSize5 + ", "
                    + " stock_size6 = stock_size6+" + invoiceSize6 + ", "
                    + " stock_size7 = stock_size7+" + invoiceSize7 + ", "
                    + " stock_size8 = stock_size8+" + invoiceSize8 + ", "
                    + " stock_size9 = stock_size9+" + invoiceSize9 + ", "
                    + " stock_sub_quantity = stock_sub_quantity+" + invoiceQuantity + ", "
                    + " update_by = '" + updateBy + "', "
                    + " update_date = CURRENT_TIMESTAMP "
                    + " WHERE "
                    + " item_code = '" + itemCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteReturnInvoice(
            Connection connection,
            String invoiceNo) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String deleteQueryStatement = " DELETE FROM "
                    + " product_return_info "
                    + " WHERE "
                    + " invoice_no = '" + invoiceNo + "' ";

            try {

                ps = connection.prepareStatement(deleteQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveInvoiceExchangeMaster(
            Connection connection,
            String invoiceNo,
            String exchangeCause,
            String customerID,
            String customerName,
            String customerMobile,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertUpdateQueryStatement = " INSERT INTO "
                    + " exchange_master( "
                    + " invoice_no, "
                    + " exchange_cause, "
                    + " customer_id, "
                    + " customer_name, "
                    + " customer_mobile, "
                    + " insert_by, "
                    + " insert_date "
                    + " )VALUES("
                    + " '" + invoiceNo + "', "
                    + " '" + exchangeCause + "', "
                    + " '" + customerID + "', "
                    + " '" + customerName + "', "
                    + " '" + customerMobile + "', "
                    + " '" + insertBy + "', "
                    + " CURRENT_TIMESTAMP)";

//                    + " ON DUPLICATE KEY UPDATE "
//                    + " s0_received = s0_received+" + size0 + ", "
//                    + " s1_received = s1_received+" + size1 + ", "
//                    + " s2_received = s2_received+" + size2 + ", "
//                    + " s3_received = s3_received+" + size3 + ", "
//                    + " s4_received = s4_received+" + size4 + ", "
//                    + " s5_received = s5_received+" + size5 + ", "
//                    + " s6_received = s6_received+" + size6 + ", "
//                    + " s7_received = s7_received+" + size7 + ", "
//                    + " s8_received = s8_received+" + size8 + ", "
//                    + " s9_received = s9_received+" + size9 + ", "
//                    + " receive_quantity = receive_quantity +" + quantity + ", "
//                    + " receive_sub_total = receive_sub_total +" + subTotal + ", "
//                    + " received_by = '" + receiveBy + "', "
//                    + " receive_date = CURRENT_TIMESTAMP";
//            
            try {

                ps = connection.prepareStatement(insertUpdateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int saveInvoiceExchangeChild(
            Connection connection,
            String invoiceNo,
            String oldItemCode,
            String oldSizeUnit,
            Integer oldSize0,
            Integer oldSize1,
            Integer oldSize2,
            Integer oldSize3,
            Integer oldSize4,
            Integer oldSize5,
            Integer oldSize6,
            Integer oldSize7,
            Integer oldSize8,
            Integer oldSize9,
            Integer oldQuantity,
            Double oldMrp,
            Double oldTotal,
            String newItemCode,
            String newSizeUnit,
            Integer newSize0,
            Integer newSize1,
            Integer newSize2,
            Integer newSize3,
            Integer newSize4,
            Integer newSize5,
            Integer newSize6,
            Integer newSize7,
            Integer newSize8,
            Integer newSize9,
            Integer newQuantity,
            Double newMrp,
            Double newTotal) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String insertUpdateQueryStatement = " INSERT INTO "
                    + " exchange_child( "
                    + " invoice_no, "
                    + " old_item_code, "
                    + " old_size_unit, "
                    + " old_size0, "
                    + " old_size1, "
                    + " old_size2, "
                    + " old_size3, "
                    + " old_size4, "
                    + " old_size5, "
                    + " old_size6, "
                    + " old_size7, "
                    + " old_size8, "
                    + " old_size9, "
                    + " old_quantity, "
                    + " old_mrp, "
                    + " old_total, "
                    + " new_item_code, "
                    + " new_size_unit, "
                    + " new_size0, "
                    + " new_size1, "
                    + " new_size2, "
                    + " new_size3, "
                    + " new_size4, "
                    + " new_size5, "
                    + " new_size6, "
                    + " new_size7, "
                    + " new_size8, "
                    + " new_size9, "
                    + " new_quantity, "
                    + " new_mrp, "
                    + " new_total "
                    + " )VALUES("
                    + " '" + invoiceNo + "', "
                    + " '" + oldItemCode + "', "
                    + " '" + oldSizeUnit + "', "
                    + " '" + oldSize0 + "', "
                    + " '" + oldSize1 + "', "
                    + " '" + oldSize2 + "', "
                    + " '" + oldSize3 + "', "
                    + " '" + oldSize4 + "', "
                    + " '" + oldSize5 + "', "
                    + " '" + oldSize6 + "', "
                    + " '" + oldSize7 + "', "
                    + " '" + oldSize8 + "', "
                    + " '" + oldSize9 + "', "
                    + " '" + oldQuantity + "', "
                    + " '" + oldMrp + "', "
                    + " '" + oldTotal + "', "
                    + " '" + newItemCode + "', "
                    + " '" + newSizeUnit + "', "
                    + " '" + newSize0 + "', "
                    + " '" + newSize1 + "', "
                    + " '" + newSize2 + "', "
                    + " '" + newSize3 + "', "
                    + " '" + newSize4 + "', "
                    + " '" + newSize5 + "', "
                    + " '" + newSize6 + "', "
                    + " '" + newSize7 + "', "
                    + " '" + newSize8 + "', "
                    + " '" + newSize9 + "', "
                    + " '" + newQuantity + "', "
                    + " '" + newMrp + "', "
                    + " '" + newTotal + "')";

            try {

                ps = connection.prepareStatement(insertUpdateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateInvoiceForExchange(
            Connection connection,
            String itemCode,
            String invoice,
            Integer invoiceSize0,
            Integer invoiceSize1,
            Integer invoiceSize2,
            Integer invoiceSize3,
            Integer invoiceSize4,
            Integer invoiceSize5,
            Integer invoiceSize6,
            Integer invoiceSize7,
            Integer invoiceSize8,
            Integer invoiceSize9,
            Integer invoiceQuantity,
            Double mrp,
            Double total,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " UPDATE "
                    + " invoice_master IVM, "
                    + " invoice_child IVC "
                    + " SET "
                    + " IVM.grand_total = '" + total + "', "
                    + " IVM.update_by = '" + updateBy + "', "
                    + " IVM.update_date = CURRENT_TIMESTAMP, "
                    + " IVC.invoice_size0 = IVC.invoice_size0-" + invoiceSize0 + ", "
                    + " IVC.invoice_size1 = IVC.invoice_size1-" + invoiceSize1 + ", "
                    + " IVC.invoice_size2 = IVC.invoice_size2-" + invoiceSize2 + ", "
                    + " IVC.invoice_size3 = IVC.invoice_size3-" + invoiceSize3 + ", "
                    + " IVC.invoice_size4 = IVC.invoice_size4-" + invoiceSize4 + ", "
                    + " IVC.invoice_size5 = IVC.invoice_size5-" + invoiceSize5 + ", "
                    + " IVC.invoice_size6 = IVC.invoice_size6-" + invoiceSize6 + ", "
                    + " IVC.invoice_size7 = IVC.invoice_size7-" + invoiceSize7 + ", "
                    + " IVC.invoice_size8 = IVC.invoice_size8-" + invoiceSize8 + ", "
                    + " IVC.invoice_size9 = IVC.invoice_size9-" + invoiceSize9 + ", "
                    + " IVC.invoice_quantity = IVC.invoice_quantity-" + invoiceQuantity + ", "
                    + " IVC.invoice_sub_total = IVC.invoice_sub_total-" + (mrp * invoiceQuantity) + " "
                    + " WHERE "
                    + " IVM.invoice_no = IVC.invoice_no "
                    + " AND "
                    + " IVM.invoice_no = '" + invoice + "' "
                    + " AND "
                    + " IVC.item_code = '" + itemCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int insertInvoiceForExchange(
            Connection connection,
            String itemCode,
            String invoice,
            Integer invoiceSize0,
            Integer invoiceSize1,
            Integer invoiceSize2,
            Integer invoiceSize3,
            Integer invoiceSize4,
            Integer invoiceSize5,
            Integer invoiceSize6,
            Integer invoiceSize7,
            Integer invoiceSize8,
            Integer invoiceSize9,
            Integer invoiceQuantity,
            Double subTotal,
            Double grandTotal,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            String updateQueryStatement = " INSERT INTO "
                    + " invoice_master( SELECT ), "
                    + " invoice_child IVC "
                    + " SET "
                    + " IVM.grand_total = '" + grandTotal + "', "
                    + " IVM.update_by = '" + updateBy + "', "
                    + " IVM.update_date = CURRENT_TIMESTAMP, "
                    + " IVC.invoice_size0 = IVC.invoice_size0-" + invoiceSize0 + ", "
                    + " IVC.invoice_size1 = IVC.invoice_size1-" + invoiceSize1 + ", "
                    + " IVC.invoice_size2 = IVC.invoice_size2-" + invoiceSize2 + ", "
                    + " IVC.invoice_size3 = IVC.invoice_size3-" + invoiceSize3 + ", "
                    + " IVC.invoice_size4 = IVC.invoice_size4-" + invoiceSize4 + ", "
                    + " IVC.invoice_size5 = IVC.invoice_size5-" + invoiceSize5 + ", "
                    + " IVC.invoice_size6 = IVC.invoice_size6-" + invoiceSize6 + ", "
                    + " IVC.invoice_size7 = IVC.invoice_size7-" + invoiceSize7 + ", "
                    + " IVC.invoice_size8 = IVC.invoice_size8-" + invoiceSize8 + ", "
                    + " IVC.invoice_size9 = IVC.invoice_size9-" + invoiceSize9 + ", "
                    + " IVC.invoice_quantity = IVC.invoice_quantity-" + invoiceQuantity + ", "
                    + " IVC.invoice_sub_total = IVC.invoice_sub_total-" + subTotal + " "
                    + " WHERE "
                    + " IVM.invoice_no = IVC.invoice_no "
                    + " AND "
                    + " IVM.invoice_no = '" + invoice + "' "
                    + " AND "
                    + " IVC.item_code = '" + itemCode + "' ";

            try {

                ps = connection.prepareStatement(updateQueryStatement);

                status = ps.executeUpdate();
            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public static void main(String[] args) {
        Connection c = DBConnection.getMySqlConnection();
        InfoEntry i = new InfoEntry();
        int p = i.deleteSubCategoryInfo(c, "rf");
        System.out.println(p);
    }
}
