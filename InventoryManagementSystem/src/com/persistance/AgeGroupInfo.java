package com.persistance;

public class AgeGroupInfo {

    private String ageCode;
    private String ageCodeName;

    /**
     * @return the ageCode
     */
    public String getAgeCode() {
        return ageCode;
    }

    /**
     * @param ageCode the ageCode to set
     */
    public void setAgeCode(String ageCode) {
        this.ageCode = ageCode;
    }

    /**
     * @return the ageCodeName
     */
    public String getAgeCodeName() {
        return ageCodeName;
    }

    /**
     * @param ageCodeName the ageCodeName to set
     */
    public void setAgeCodeName(String ageCodeName) {
        this.ageCodeName = ageCodeName;
    }
}
