package com.persistance;

public class ExchangeMasterInfo {

    private Integer exchangeMasterID;
    private String invoiceNo;
    private String exchangeCause;
    private String customerID;
    private String customerName;
    private String customerMobile;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private Character exchangeStatus;
    private ExchangeChildInfo exchangeChild;

    /**
     * @return the exchangeMasterID
     */
    public Integer getExchangeMasterID() {
        return exchangeMasterID;
    }

    /**
     * @param exchangeMasterID the exchangeMasterID to set
     */
    public void setExchangeMasterID(Integer exchangeMasterID) {
        this.exchangeMasterID = exchangeMasterID;
    }

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * @return the exchangeCause
     */
    public String getExchangeCause() {
        return exchangeCause;
    }

    /**
     * @param exchangeCause the exchangeCause to set
     */
    public void setExchangeCause(String exchangeCause) {
        this.exchangeCause = exchangeCause;
    }

    /**
     * @return the customerID
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerMobile
     */
    public String getCustomerMobile() {
        return customerMobile;
    }

    /**
     * @param customerMobile the customerMobile to set
     */
    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the exchangeStatus
     */
    public Character getExchangeStatus() {
        return exchangeStatus;
    }

    /**
     * @param exchangeStatus the exchangeStatus to set
     */
    public void setExchangeStatus(Character exchangeStatus) {
        this.exchangeStatus = exchangeStatus;
    }

    /**
     * @return the exchangeChild
     */
    public ExchangeChildInfo getExchangeChild() {
        return exchangeChild;
    }

    /**
     * @param exchangeChild the exchangeChild to set
     */
    public void setExchangeChild(ExchangeChildInfo exchangeChild) {
        this.exchangeChild = exchangeChild;
    }
}
