package com.persistance;

public class InvoiceMasterInfo {

    private String invoiceNo;
    private String customerID;
    private String customerName;
    private String customerMobile;
    private String customerAddress;
    private Character invoiceStatus;
    private String invoiceBy;
    private String invoiceDate;
    private Double discount;
    private Double grandTotal;
    private Integer deliveryCharge;
    private Integer advance;
    private InvoiceChildInfo invoiceChildInfo;

    /**
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * @param invoiceNo the invoiceNo to set
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * @return the customerID
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerMobile
     */
    public String getCustomerMobile() {
        return customerMobile;
    }

    /**
     * @param customerMobile the customerMobile to set
     */
    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    /**
     * @return the customerAddress
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * @param customerAddress the customerAddress to set
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * @return the invoiceStatus
     */
    public Character getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * @param invoiceStatus the invoiceStatus to set
     */
    public void setInvoiceStatus(Character invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    /**
     * @return the invoiceBy
     */
    public String getInvoiceBy() {
        return invoiceBy;
    }

    /**
     * @param invoiceBy the invoiceBy to set
     */
    public void setInvoiceBy(String invoiceBy) {
        this.invoiceBy = invoiceBy;
    }

    /**
     * @return the invoiceDate
     */
    public String getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * @param invoiceDate the invoiceDate to set
     */
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * @return the discount
     */
    public Double getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    /**
     * @return the grandTotal
     */
    public Double getGrandTotal() {
        return grandTotal;
    }

    /**
     * @param grandTotal the grandTotal to set
     */
    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    /**
     * @return the deliveryCharge
     */
    public Integer getDeliveryCharge() {
        return deliveryCharge;
    }

    /**
     * @param deliveryCharge the deliveryCharge to set
     */
    public void setDeliveryCharge(Integer deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    /**
     * @return the advance
     */
    public Integer getAdvance() {
        return advance;
    }

    /**
     * @param advance the advance to set
     */
    public void setAdvance(Integer advance) {
        this.advance = advance;
    }

    /**
     * @return the invoiceChildInfo
     */
    public InvoiceChildInfo getInvoiceChildInfo() {
        return invoiceChildInfo;
    }

    /**
     * @param invoiceChildInfo the invoiceChildInfo to set
     */
    public void setInvoiceChildInfo(InvoiceChildInfo invoiceChildInfo) {
        this.invoiceChildInfo = invoiceChildInfo;
    }
}
