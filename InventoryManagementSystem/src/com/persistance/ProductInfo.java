package com.persistance;

import java.util.Date;

public class ProductInfo {

    private Integer itemId;
    private String productName;
    private String itemCode;
    private Double itemCostPrice;
    private Double itemVat;
    private Double itemNetPrice;
    private Double itemMRP;
    private String itemSupplier;
    private String itemSuppReference;
    private String itemState;
    private String itemTemperature;
    private Date itemLifeCycle;
    private String itemDescription;
    private String itemImageURL;
    private Character itemStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private SizeInfo sizeInfo;
    private ColorInfo colorInfo;
    private BrandInfo brandInfo;
    private GenderGroupInfo genderGroupInfo;
    private AgeGroupInfo ageGroupInfo;
    private CategoryInfo categoryInfo;
    private SubCategoryInfo subCategoryInfo;
    private CompanyInfo companyInfo;
    private CurrentStockInfo currentStockInfo;
    private ProductOrderDetailsInfo productOrderDetailsInfo;

    /**
     * @return the itemId
     */
    public Integer getItemId() {
        return itemId;
    }

    /**
     * @param itemId the itemId to set
     */
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemCostPrice
     */
    public Double getItemCostPrice() {
        return itemCostPrice;
    }

    /**
     * @param itemCostPrice the itemCostPrice to set
     */
    public void setItemCostPrice(Double itemCostPrice) {
        this.itemCostPrice = itemCostPrice;
    }

    /**
     * @return the itemVat
     */
    public Double getItemVat() {
        return itemVat;
    }

    /**
     * @param itemVat the itemVat to set
     */
    public void setItemVat(Double itemVat) {
        this.itemVat = itemVat;
    }

    /**
     * @return the itemNetPrice
     */
    public Double getItemNetPrice() {
        return itemNetPrice;
    }

    /**
     * @param itemNetPrice the itemNetPrice to set
     */
    public void setItemNetPrice(Double itemNetPrice) {
        this.itemNetPrice = itemNetPrice;
    }

    /**
     * @return the itemMRP
     */
    public Double getItemMRP() {
        return itemMRP;
    }

    /**
     * @param itemMRP the itemMRP to set
     */
    public void setItemMRP(Double itemMRP) {
        this.itemMRP = itemMRP;
    }

    /**
     * @return the itemSupplier
     */
    public String getItemSupplier() {
        return itemSupplier;
    }

    /**
     * @param itemSupplier the itemSupplier to set
     */
    public void setItemSupplier(String itemSupplier) {
        this.itemSupplier = itemSupplier;
    }

    /**
     * @return the itemSuppReference
     */
    public String getItemSuppReference() {
        return itemSuppReference;
    }

    /**
     * @param itemSuppReference the itemSuppReference to set
     */
    public void setItemSuppReference(String itemSuppReference) {
        this.itemSuppReference = itemSuppReference;
    }

    /**
     * @return the itemState
     */
    public String getItemState() {
        return itemState;
    }

    /**
     * @param itemState the itemState to set
     */
    public void setItemState(String itemState) {
        this.itemState = itemState;
    }

    /**
     * @return the itemTemperature
     */
    public String getItemTemperature() {
        return itemTemperature;
    }

    /**
     * @param itemTemperature the itemTemperature to set
     */
    public void setItemTemperature(String itemTemperature) {
        this.itemTemperature = itemTemperature;
    }

    /**
     * @return the itemLifeCycle
     */
    public Date getItemLifeCycle() {
        return itemLifeCycle;
    }

    /**
     * @param itemLifeCycle the itemLifeCycle to set
     */
    public void setItemLifeCycle(Date itemLifeCycle) {
        this.itemLifeCycle = itemLifeCycle;
    }

    /**
     * @return the itemDescription
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * @param itemDescription the itemDescription to set
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * @return the itemImageURL
     */
    public String getItemImageURL() {
        return itemImageURL;
    }

    /**
     * @param itemImageURL the itemImageURL to set
     */
    public void setItemImageURL(String itemImageURL) {
        this.itemImageURL = itemImageURL;
    }

    /**
     * @return the itemStatus
     */
    public Character getItemStatus() {
        return itemStatus;
    }

    /**
     * @param itemStatus the itemStatus to set
     */
    public void setItemStatus(Character itemStatus) {
        this.itemStatus = itemStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the sizeInfo
     */
    public SizeInfo getSizeInfo() {
        return sizeInfo;
    }

    /**
     * @param sizeInfo the sizeInfo to set
     */
    public void setSizeInfo(SizeInfo sizeInfo) {
        this.sizeInfo = sizeInfo;
    }

    /**
     * @return the colorInfo
     */
    public ColorInfo getColorInfo() {
        return colorInfo;
    }

    /**
     * @param colorInfo the colorInfo to set
     */
    public void setColorInfo(ColorInfo colorInfo) {
        this.colorInfo = colorInfo;
    }

    /**
     * @return the brandInfo
     */
    public BrandInfo getBrandInfo() {
        return brandInfo;
    }

    /**
     * @param brandInfo the brandInfo to set
     */
    public void setBrandInfo(BrandInfo brandInfo) {
        this.brandInfo = brandInfo;
    }

    /**
     * @return the genderGroupInfo
     */
    public GenderGroupInfo getGenderGroupInfo() {
        return genderGroupInfo;
    }

    /**
     * @param genderGroupInfo the genderGroupInfo to set
     */
    public void setGenderGroupInfo(GenderGroupInfo genderGroupInfo) {
        this.genderGroupInfo = genderGroupInfo;
    }

    /**
     * @return the ageGroupInfo
     */
    public AgeGroupInfo getAgeGroupInfo() {
        return ageGroupInfo;
    }

    /**
     * @param ageGroupInfo the ageGroupInfo to set
     */
    public void setAgeGroupInfo(AgeGroupInfo ageGroupInfo) {
        this.ageGroupInfo = ageGroupInfo;
    }

    /**
     * @return the categoryInfo
     */
    public CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    /**
     * @param categoryInfo the categoryInfo to set
     */
    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    /**
     * @return the subCategoryInfo
     */
    public SubCategoryInfo getSubCategoryInfo() {
        return subCategoryInfo;
    }

    /**
     * @param subCategoryInfo the subCategoryInfo to set
     */
    public void setSubCategoryInfo(SubCategoryInfo subCategoryInfo) {
        this.subCategoryInfo = subCategoryInfo;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }

    /**
     * @return the currentStockInfo
     */
    public CurrentStockInfo getCurrentStockInfo() {
        return currentStockInfo;
    }

    /**
     * @param currentStockInfo the currentStockInfo to set
     */
    public void setCurrentStockInfo(CurrentStockInfo currentStockInfo) {
        this.currentStockInfo = currentStockInfo;
    }

    /**
     * @return the productOrderDetailsInfo
     */
    public ProductOrderDetailsInfo getProductOrderDetailsInfo() {
        return productOrderDetailsInfo;
    }

    /**
     * @param productOrderDetailsInfo the productOrderDetailsInfo to set
     */
    public void setProductOrderDetailsInfo(ProductOrderDetailsInfo productOrderDetailsInfo) {
        this.productOrderDetailsInfo = productOrderDetailsInfo;
    }
}
