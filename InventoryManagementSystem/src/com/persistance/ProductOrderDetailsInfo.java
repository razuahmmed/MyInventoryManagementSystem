package com.persistance;

public class ProductOrderDetailsInfo {

    private Integer orderDetailsID;
    private String itemCode;
    private Character sizeCode;
    private Double netPrice;
    private Integer quantity;
    private Integer s0;
    private Integer s1;
    private Integer s2;
    private Integer s3;
    private Integer s4;
    private Integer s5;
    private Integer s6;
    private Integer s7;
    private Integer s8;
    private Integer s9;
    private Double subTotal;
    private Character orderStatus;
    private String orderDate;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;

    private OrderReceiveInfo orderReceiveInfo;
    private SizeInfo sizeInfo;

    /**
     * @return the subTotal
     */
    public Double getSubTotal() {
        return subTotal;
    }

    /**
     * @param subTotal the subTotal to set
     */
    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * @return the orderDetailsID
     */
    public Integer getOrderDetailsID() {
        return orderDetailsID;
    }

    /**
     * @param orderDetailsID the orderDetailsID to set
     */
    public void setOrderDetailsID(Integer orderDetailsID) {
        this.orderDetailsID = orderDetailsID;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode the itemCode to set
     */
    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the sizeCode
     */
    public Character getSizeCode() {
        return sizeCode;
    }

    /**
     * @param sizeCode the sizeCode to set
     */
    public void setSizeCode(Character sizeCode) {
        this.sizeCode = sizeCode;
    }

    /**
     * @return the netPrice
     */
    public Double getNetPrice() {
        return netPrice;
    }

    /**
     * @param netPrice the netPrice to set
     */
    public void setNetPrice(Double netPrice) {
        this.netPrice = netPrice;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the s0
     */
    public Integer getS0() {
        return s0;
    }

    /**
     * @param s0 the s0 to set
     */
    public void setS0(Integer s0) {
        this.s0 = s0;
    }

    /**
     * @return the s1
     */
    public Integer getS1() {
        return s1;
    }

    /**
     * @param s1 the s1 to set
     */
    public void setS1(Integer s1) {
        this.s1 = s1;
    }

    /**
     * @return the s2
     */
    public Integer getS2() {
        return s2;
    }

    /**
     * @param s2 the s2 to set
     */
    public void setS2(Integer s2) {
        this.s2 = s2;
    }

    /**
     * @return the s3
     */
    public Integer getS3() {
        return s3;
    }

    /**
     * @param s3 the s3 to set
     */
    public void setS3(Integer s3) {
        this.s3 = s3;
    }

    /**
     * @return the s4
     */
    public Integer getS4() {
        return s4;
    }

    /**
     * @param s4 the s4 to set
     */
    public void setS4(Integer s4) {
        this.s4 = s4;
    }

    /**
     * @return the s5
     */
    public Integer getS5() {
        return s5;
    }

    /**
     * @param s5 the s5 to set
     */
    public void setS5(Integer s5) {
        this.s5 = s5;
    }

    /**
     * @return the s6
     */
    public Integer getS6() {
        return s6;
    }

    /**
     * @param s6 the s6 to set
     */
    public void setS6(Integer s6) {
        this.s6 = s6;
    }

    /**
     * @return the s7
     */
    public Integer getS7() {
        return s7;
    }

    /**
     * @param s7 the s7 to set
     */
    public void setS7(Integer s7) {
        this.s7 = s7;
    }

    /**
     * @return the s8
     */
    public Integer getS8() {
        return s8;
    }

    /**
     * @param s8 the s8 to set
     */
    public void setS8(Integer s8) {
        this.s8 = s8;
    }

    /**
     * @return the s9
     */
    public Integer getS9() {
        return s9;
    }

    /**
     * @param s9 the s9 to set
     */
    public void setS9(Integer s9) {
        this.s9 = s9;
    }

    /**
     * @return the orderStatus
     */
    public Character getOrderStatus() {
        return orderStatus;
    }

    /**
     * @param orderStatus the orderStatus to set
     */
    public void setOrderStatus(Character orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * @return the orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the orderReceiveInfo
     */
    public OrderReceiveInfo getOrderReceiveInfo() {
        return orderReceiveInfo;
    }

    /**
     * @param orderReceiveInfo the orderReceiveInfo to set
     */
    public void setOrderReceiveInfo(OrderReceiveInfo orderReceiveInfo) {
        this.orderReceiveInfo = orderReceiveInfo;
    }

    /**
     * @return the sizeInfo
     */
    public SizeInfo getSizeInfo() {
        return sizeInfo;
    }

    /**
     * @param sizeInfo the sizeInfo to set
     */
    public void setSizeInfo(SizeInfo sizeInfo) {
        this.sizeInfo = sizeInfo;
    }
}
