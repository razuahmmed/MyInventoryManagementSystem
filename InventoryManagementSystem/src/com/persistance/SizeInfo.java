package com.persistance;

public class SizeInfo {

    private Character sizeCode;
    private String sizeUnit;
    private String size0;
    private String size1;
    private String size2;
    private String size3;
    private String size4;
    private String size5;
    private String size6;
    private String size7;
    private String size8;
    private String size9;
    private String sizeDescription;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private Character sizeStatus;
    private CompanyInfo companyInfo;

    /**
     * @return the sizeCode
     */
    public Character getSizeCode() {
        return sizeCode;
    }

    /**
     * @param sizeCode the sizeCode to set
     */
    public void setSizeCode(Character sizeCode) {
        this.sizeCode = sizeCode;
    }

    /**
     * @return the sizeUnit
     */
    public String getSizeUnit() {
        return sizeUnit;
    }

    /**
     * @param sizeUnit the sizeUnit to set
     */
    public void setSizeUnit(String sizeUnit) {
        this.sizeUnit = sizeUnit;
    }

    /**
     * @return the size0
     */
    public String getSize0() {
        return size0;
    }

    /**
     * @param size0 the size0 to set
     */
    public void setSize0(String size0) {
        this.size0 = size0;
    }

    /**
     * @return the size1
     */
    public String getSize1() {
        return size1;
    }

    /**
     * @param size1 the size1 to set
     */
    public void setSize1(String size1) {
        this.size1 = size1;
    }

    /**
     * @return the size2
     */
    public String getSize2() {
        return size2;
    }

    /**
     * @param size2 the size2 to set
     */
    public void setSize2(String size2) {
        this.size2 = size2;
    }

    /**
     * @return the size3
     */
    public String getSize3() {
        return size3;
    }

    /**
     * @param size3 the size3 to set
     */
    public void setSize3(String size3) {
        this.size3 = size3;
    }

    /**
     * @return the size4
     */
    public String getSize4() {
        return size4;
    }

    /**
     * @param size4 the size4 to set
     */
    public void setSize4(String size4) {
        this.size4 = size4;
    }

    /**
     * @return the size5
     */
    public String getSize5() {
        return size5;
    }

    /**
     * @param size5 the size5 to set
     */
    public void setSize5(String size5) {
        this.size5 = size5;
    }

    /**
     * @return the size6
     */
    public String getSize6() {
        return size6;
    }

    /**
     * @param size6 the size6 to set
     */
    public void setSize6(String size6) {
        this.size6 = size6;
    }

    /**
     * @return the size7
     */
    public String getSize7() {
        return size7;
    }

    /**
     * @param size7 the size7 to set
     */
    public void setSize7(String size7) {
        this.size7 = size7;
    }

    /**
     * @return the size8
     */
    public String getSize8() {
        return size8;
    }

    /**
     * @param size8 the size8 to set
     */
    public void setSize8(String size8) {
        this.size8 = size8;
    }

    /**
     * @return the size9
     */
    public String getSize9() {
        return size9;
    }

    /**
     * @param size9 the size9 to set
     */
    public void setSize9(String size9) {
        this.size9 = size9;
    }

    /**
     * @return the sizeDescription
     */
    public String getSizeDescription() {
        return sizeDescription;
    }

    /**
     * @param sizeDescription the sizeDescription to set
     */
    public void setSizeDescription(String sizeDescription) {
        this.sizeDescription = sizeDescription;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the sizeStatus
     */
    public Character getSizeStatus() {
        return sizeStatus;
    }

    /**
     * @param sizeStatus the sizeStatus to set
     */
    public void setSizeStatus(Character sizeStatus) {
        this.sizeStatus = sizeStatus;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }
}
