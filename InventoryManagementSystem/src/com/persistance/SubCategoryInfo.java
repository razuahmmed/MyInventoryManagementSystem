package com.persistance;

public class SubCategoryInfo {

    private String subCategoryCode;
    private String subCategoryName;
    private String subCategoryDescription;
    private Character subCategoryStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private CategoryInfo categoryInfo;
    private CompanyInfo companyInfo;

    /**
     * @return the subCategoryCode
     */
    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    /**
     * @param subCategoryCode the subCategoryCode to set
     */
    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    /**
     * @return the subCategoryName
     */
    public String getSubCategoryName() {
        return subCategoryName;
    }

    /**
     * @param subCategoryName the subCategoryName to set
     */
    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    /**
     * @return the subCategoryDescription
     */
    public String getSubCategoryDescription() {
        return subCategoryDescription;
    }

    /**
     * @param subCategoryDescription the subCategoryDescription to set
     */
    public void setSubCategoryDescription(String subCategoryDescription) {
        this.subCategoryDescription = subCategoryDescription;
    }

    /**
     * @return the subCategoryStatus
     */
    public Character getSubCategoryStatus() {
        return subCategoryStatus;
    }

    /**
     * @param subCategoryStatus the subCategoryStatus to set
     */
    public void setSubCategoryStatus(Character subCategoryStatus) {
        this.subCategoryStatus = subCategoryStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the categoryInfo
     */
    public CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }

    /**
     * @param categoryInfo the categoryInfo to set
     */
    public void setCategoryInfo(CategoryInfo categoryInfo) {
        this.categoryInfo = categoryInfo;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }
}
